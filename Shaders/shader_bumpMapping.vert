#version 330 core

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inColor;	
layout (location = 2) in vec3 inNormal;
layout (location = 3) in vec2 inTexCoord;
layout (location = 4) in vec3 inTangent;

uniform mat4 modelViewProj;
uniform mat4 normal;
uniform mat4 modelView;

out vec3 pv;
out vec3 nv;
out vec3 color;
out vec2 texCoord;
out vec3 T;
out vec3 B;

void main()
{
	pv = (modelView * vec4(inPos, 1.0)).xyz;;     
	nv = normalize((normal * vec4(inNormal,0)).xyz);	

	color = inColor;
	texCoord = inTexCoord;	
	
	T = (modelView * vec4(inTangent, 0.0)).xyz;

	gl_Position = modelViewProj * vec4(inPos, 1.0);
}
