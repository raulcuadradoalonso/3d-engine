#version 430 core

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gDiffuse;

uniform sampler2D colorTex;
uniform sampler2D emiTex;

void main()
{    
    // store the fragment position vector in the first gbuffer texture
    outPosition = pos;
    // also store the per-fragment normals into the gbuffer
    gNormal = normalize(norm);
    // and the diffuse per-fragment color
    gDiffuse = texture(colorTex, texCoord).rgb;
}  
