#version 430 core

in vec2 texCoord;

out vec4 outColor;

uniform sampler2D ds_positionTex;
uniform sampler2D ds_colorTex;
uniform sampler2D ds_normalTex;

vec3 pos;
vec3 color;

//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
vec3 N;
float alpha = 500.0;
vec3 Ke;

//Propiedades de la luz
vec3 Ia = vec3 (0.3);
vec3 Id = vec3 (0.5);
vec3 Is = vec3 (1.0);
vec3 light_pos = vec3 (0.0); 

vec3 shade();

void main()
{    
	ivec2 fragCoord = ivec2(gl_FragCoord.xy);
	pos = texelFetch(ds_positionTex, fragCoord, 0).xyz;
	color = texelFetch(ds_colorTex, fragCoord, 0).xyz;
	N = texelFetch(ds_normalTex, fragCoord, 0).xyz;

	if (dot(N, N) == 0)
	{
		discard;
	}

	Ka = color;
	Kd = color;
	Ke = vec3(0.0); 
	Ks = vec3(1.0);
	
	outColor = vec4(shade(), 1.0);
}  

vec3 shade()
{
	vec3 c = vec3(0.0);
	c = Ia * Ka;

	vec3 L = normalize (light_pos - pos);
	vec3 diffuse = Id * Kd * dot (L,N);
	c += clamp(diffuse, 0.0, 1.0);
	
	vec3 V = normalize (-pos); 
	vec3 R = normalize (reflect (-L,N)); 
	float factor = max (dot (R,V), 0.01); 
	vec3 specular = Is*Ks*pow(factor,alpha); 
	c += clamp(specular, 0.0, 1.0); 

	c += Ke;

	return c;
}