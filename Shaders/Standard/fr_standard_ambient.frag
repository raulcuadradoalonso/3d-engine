#version 330 core

#define NUM_LIGHTS 1
layout(location = 0) out vec4 outColor;

in vec2 texCoord;
in vec3 color;

uniform sampler2D colorTex;

//Object properties
vec3 Ka;
uniform vec3 I_ambient;

//Status
uniform bool renderToFBO;

void main()
{
	Ka = texture(colorTex, texCoord).rgb;
	
	outColor = vec4(I_ambient * Ka * color, 1.0);
}