#version 330 core

#define NUM_LIGHTS 1
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outVertex;
layout(location = 2) out float outDepth;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

//Object properties
uniform sampler2D colorTex;
vec3 Ka;
vec3 Kd;
vec3 Ks;
vec3 N;
vec3 Ke;
float alpha = 5000.0;

//Light properties
uniform vec3 lightPos[NUM_LIGHTS];
uniform vec3 I_diffuse[NUM_LIGHTS];
uniform vec3 I_specular[NUM_LIGHTS];
uniform float I_range[NUM_LIGHTS];
uniform float I_intensity[NUM_LIGHTS];

uniform mat4 view;

//Status
uniform bool renderToFBO;

//Formula from https://imdoingitwrong.wordpress.com/2011/01/31/light-attenuation/
float attenuation(float range, float dist)
{
	float dist2 = pow(dist, 2);
	float range2 = pow(range, 2);

	return min (1/(1 + 2*dist/range + dist2/range2), 1.0);
}

vec3 shade()
{
	vec3 c = vec3(0.0);

	for (int i = 0; i < NUM_LIGHTS; i++)
	{	
		vec3 currentLightPos = (view * vec4(lightPos[i], 1.0)).xyz;
		//vec3 currentLightPos = lightPos[i];

		//Light attenuation
		float distanceToLight = length(currentLightPos - pos);
		float att = I_intensity[i] * attenuation (I_range[i], distanceToLight);		

		vec3 L = normalize (currentLightPos - pos);
		vec3 diffuse = I_diffuse[i] * att * Kd * dot (L,N);
		c += clamp(diffuse, 0.0, 1.0);
	
		vec3 V = normalize (-pos);
		vec3 R = normalize (reflect(-L,N));
		float factor = max (dot(R,V), 0.01);
		vec3 specular = I_specular[i] * att * Ks * pow(factor,alpha);
		c += clamp(specular, 0.0, 1.0);		
	}
	
	return c;
}


void main()
{
	Ka = texture(colorTex, texCoord).rgb;
	Kd = texture(colorTex, texCoord).rgb;
	Ks = vec3 (1.0);

	N = normalize (norm);
	
	outColor = vec4(shade(), 1.0);

	if (renderToFBO)
	{
		outVertex = vec4(pos, 1.0);
		outDepth = gl_FragDepth;
	}
}