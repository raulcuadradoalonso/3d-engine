#version 330 core

#define NUM_LIGHTS 2
out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

uniform sampler2D colorTex;
uniform sampler2D emiTex;


//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
vec3 N;
vec3 Ke;
float alpha = 5000.0;

//Propiedades de la luz
uniform vec3 light_position[NUM_LIGHTS];
uniform vec3 I_ambient[NUM_LIGHTS];
uniform vec3 I_diffuse[NUM_LIGHTS];
uniform vec3 I_specular[NUM_LIGHTS];


vec3 shade();

void main()
{
	Ka = texture(colorTex, texCoord).rgb;
	Kd = texture(colorTex, texCoord).rgb;
	Ke = texture(emiTex, texCoord).rgb;
	Ks = vec3 (1.0);

	N = normalize (norm);
	
	outColor = vec4(shade(), 1.0);   
}

vec3 shade()
{
	vec3 c = vec3(0.0);

	for (int i = 0; i < NUM_LIGHTS; i++)
	{		
		c += I_ambient[i] * Ka;

		vec3 L = normalize (light_position[i] - pos);
		vec3 diffuse = I_diffuse[i] * Kd * dot (L,N);
		c += clamp(diffuse, 0.0, 1.0);
	
		vec3 V = normalize (-pos);
		vec3 R = normalize (reflect (-L,N));
		float factor = max (dot (R,V), 0.01);
		vec3 specular = I_specular[i]*Ks*pow(factor,alpha);
		c += clamp(specular, 0.0, 1.0);		
	}

	c += Ke;
	
	return c;
}
