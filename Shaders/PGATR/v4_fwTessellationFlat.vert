#version 430 core

layout (location = 0) in vec3 inPos;	
layout (location = 2) in vec3 inNormal;
layout (location = 3) in vec2 texCoord;

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;

out vec3 in_fg_pos;
out vec3 in_fg_norm;
out vec2 in_fg_texCoord;


void main()
{
	in_fg_norm = (normal * vec4(inNormal, 0.0)).xyz;
	in_fg_pos = (modelView * vec4(inPos, 1.0)).xyz;
	in_fg_texCoord = texCoord;
	
	gl_Position =  modelViewProj* vec4(inPos,1.0);
}