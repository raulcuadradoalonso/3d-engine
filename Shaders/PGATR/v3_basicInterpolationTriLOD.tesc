#version 430 core

layout(vertices = 3) out;
uniform mat4 modelView;

void main(void)
{
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

	vec4 p1 = modelView * gl_in[0].gl_Position;
	vec4 p2 = modelView * gl_in[1].gl_Position;
	vec4 p3 = modelView * gl_in[2].gl_Position;

	float maxDis = 15;
	float minDis = 1;

	float maxLevel = 8;
	float minLevel = 2;

	vec3 V = -((p1 + p2 + p3) / 3).xyz;
	float d2 = dot(V, V);

	float maxDis2 = maxDis * maxDis;
	float minDis2 = minDis * minDis;
	float maxLevel2 = maxLevel * maxLevel;
	float minLevel2 = minLevel * minLevel;

	d2 = clamp(d2, minDis2, maxDis2);
	d2 = (d2 - minDis2) / (maxDis2 - minDis2);

	float tessLevel = (d2 * minLevel) + ((1 - d2) * maxLevel);

	gl_TessLevelOuter[0] = tessLevel;
	gl_TessLevelOuter[1] = tessLevel;
	gl_TessLevelOuter[2] = tessLevel;

	gl_TessLevelInner[0] = tessLevel;
}