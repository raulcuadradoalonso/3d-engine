#version 430 core

layout(vertices = 3) out;

in vec3 in_tc_pos[];
in vec3 in_tc_norm[];
in vec2 in_tc_texCoord[];

out vec3 in_te_pos[];
out vec3 in_te_norm[];
out vec2 in_te_texCoord[];


void main(void)
{
	in_te_pos[gl_InvocationID] = in_tc_pos[gl_InvocationID];
	in_te_norm[gl_InvocationID] = in_tc_norm[gl_InvocationID];
	in_te_texCoord[gl_InvocationID] = in_tc_texCoord[gl_InvocationID];

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

	gl_TessLevelOuter[0] = 60;
	gl_TessLevelOuter[1] = 60;
	gl_TessLevelOuter[2] = 60;

	gl_TessLevelInner[0] = 60;
}