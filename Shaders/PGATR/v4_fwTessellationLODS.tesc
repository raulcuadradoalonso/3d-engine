#version 430 core

layout(vertices = 3) out;

uniform mat4 modelView;

in vec3 in_tc_pos[];
in vec3 in_tc_norm[];
in vec2 in_tc_texCoord[];

out vec3 in_te_pos[];
out vec3 in_te_norm[];
out vec2 in_te_texCoord[];

float tess(vec3 V, float maxDis, float minDis, float maxLevel, float minLevel){
	float d2 = dot(V, V);

	float maxDis2 = maxDis * maxDis;
	float minDis2 = minDis * minDis;
	float maxLevel2 = maxLevel * maxLevel;
	float minLevel2 = minLevel * minLevel;

	d2 = clamp(d2, minDis2, maxDis2);
	d2 = (d2 - minDis2) / (maxDis2 - minDis2);

	return (d2 * minLevel) + ((1 - d2) * maxLevel);
}


void main(void)
{
	in_te_pos[gl_InvocationID] = in_tc_pos[gl_InvocationID];
	in_te_norm[gl_InvocationID] = in_tc_norm[gl_InvocationID];
	in_te_texCoord[gl_InvocationID] = in_tc_texCoord[gl_InvocationID];

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;


	float maxDis = 10;
	float minDis = 1;

	float maxLevel = 50;
	float minLevel = 3;

	vec4 p1 = modelView * gl_in[0].gl_Position;
	vec4 p2 = modelView * gl_in[1].gl_Position;
	vec4 p3 = modelView * gl_in[2].gl_Position;

	vec3 V = -((p1 + p2 + p3) / 3).xyz;
	gl_TessLevelInner[0] = tess(V, maxDis,  minDis, maxLevel, minLevel);


	int nextId = gl_InvocationID + 1;
	nextId = nextId % 3;

	vec4 px = modelView * gl_in[gl_InvocationID].gl_Position;
	vec4 py = modelView * gl_in[nextId].gl_Position;

	V = -((px + py) / 2).xyz;
	gl_TessLevelOuter[3-nextId-gl_InvocationID] = tess(V, maxDis,  minDis, maxLevel, minLevel);
}