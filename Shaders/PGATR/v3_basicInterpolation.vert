#version 430 core

layout (location = 0) in vec3 inPos;
layout (location = 2) in vec3 inNormal;

uniform mat4 normal;
uniform mat4 projectionMatrix;
uniform mat4 modelViewProj;


out vec3 norm;

void main()
{	
	norm = normalize(projectionMatrix * (normal * vec4 (inNormal, 0.0))).xyz;
	gl_Position = vec4 (inPos,1.0);
}