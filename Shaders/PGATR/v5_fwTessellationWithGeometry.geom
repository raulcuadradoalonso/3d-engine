#version 430 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 in_geom_pos[];
in vec3 in_geom_norm[];
in vec2 in_geom_texCoord[];

out vec3 in_fg_pos;
out vec3 in_fg_norm;
out vec2 in_fg_texCoord;

uniform mat4 projectionMatrix;
uniform sampler2D colorTex;

void main() 
{	
	//Pass attributes to fragment shader
	in_fg_pos = in_geom_pos[0];
	in_fg_norm = in_geom_norm[0];
	in_fg_texCoord = in_geom_texCoord[0];

	//Compute displacement
	float displacement = texture(colorTex, in_geom_texCoord[0]).g;
	displacement = displacement * 0.25f;
	gl_Position = gl_in[0].gl_Position + vec4(in_geom_norm[0] * displacement, 0.0);

	//End vertex
    gl_Position = projectionMatrix * gl_Position;
	EmitVertex();

	//Pass attributes to fragment shader
	in_fg_pos = in_geom_pos[1];
	in_fg_norm = in_geom_norm[1];
	in_fg_texCoord = in_geom_texCoord[1];

	//Compute displacement
	displacement = texture(colorTex, in_geom_texCoord[1]).g;
	displacement = displacement * 0.25f;
	gl_Position = gl_in[1].gl_Position + vec4(in_geom_norm[1] * displacement, 0.0);

	//End vertex
	gl_Position = projectionMatrix * gl_Position;
	EmitVertex();

	//Pass attributes to fragment shader
	in_fg_pos = in_geom_pos[2];
	in_fg_norm = in_geom_norm[2];
	in_fg_texCoord = in_geom_texCoord[2];

	//Compute displacement
	displacement = texture(colorTex, in_geom_texCoord[2]).g;
	displacement = displacement * 0.25f;
	gl_Position = gl_in[2].gl_Position + vec4(in_geom_norm[2] * displacement, 0.0);

	gl_Position = projectionMatrix * gl_Position;
	EmitVertex();
	EndPrimitive();
} 