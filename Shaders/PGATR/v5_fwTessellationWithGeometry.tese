#version 430 core

layout(triangles, equal_spacing, ccw) in;

in vec3 in_te_pos[];
in vec3 in_te_norm[];
in vec2 in_te_texCoord[];

out vec3 in_geom_pos;
out vec3 in_geom_norm;
out vec2 in_geom_texCoord;

uniform mat4 modelView;

float displacementFactor = 2.0;

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
   	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
   	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

void main()
{	
	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;
	float w = 1 - u - v;
	
	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;

	in_geom_pos = interpolate3D(in_te_pos[0], in_te_pos[1], in_te_pos[2]);
	in_geom_norm = interpolate3D(in_te_norm[0], in_te_norm[1], in_te_norm[2]);
	in_geom_norm = normalize(in_geom_norm);
	in_geom_texCoord = interpolate2D(in_te_texCoord[0], in_te_texCoord[1], in_te_texCoord[2]);

	//Linear interpolation
	vec4 pos = u * p0 + v * p1 + w * p2;

	gl_Position = modelView * pos;
}