#version 430 core

layout(triangles, equal_spacing, ccw) in;

uniform mat4 modelViewProj;

void main()
{	
	
	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;
	float w = 1 - u - v;

	
	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;

	//Linear interpolation
	vec4 pos = u * p0 + v * p1 + w * p2; 

	gl_Position = modelViewProj * pos;
}