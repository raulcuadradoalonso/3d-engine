#version 430 core

layout (location = 0) in vec3 inPos;	
layout (location = 2) in vec3 inNormal;
layout (location = 3) in vec2 texCoord;

uniform mat4 modelView;
uniform mat4 normal;

out vec3 in_tc_pos;
out vec3 in_tc_norm;
out vec2 in_tc_texCoord;


void main()
{
	in_tc_norm = (normal * vec4(inNormal, 0.0)).xyz;
	in_tc_pos = (modelView * vec4(inPos, 1.0)).xyz;
	in_tc_texCoord = texCoord;
	
	gl_Position =  vec4(inPos,1.0);
}