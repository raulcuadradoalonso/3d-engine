#version 430 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

in vec3 norm[3];

float offset = 0.01f;

void main() 
{   
    gl_Position = gl_in[0].gl_Position;// - vec4(norm[0] * offset, 0.0);
	EmitVertex();
	gl_Position = gl_in[1].gl_Position;// - vec4(norm[1] * offset, 0.0);;
	EmitVertex();
	gl_Position = gl_in[2].gl_Position;// - vec4(norm[2] * offset, 0.0);;
	EmitVertex();
	gl_Position = gl_in[0].gl_Position;// - vec4(norm[0] * offset, 0.0);
	EmitVertex();
	EndPrimitive();
} 