#version 430 core
in vec3 gColor;

out vec4 outColor;

void main()
{
	outColor = vec4(gColor.rgb, 1.0);
	//outColor = vec4(1.0, 0.0, 0.0, 1.0);
}