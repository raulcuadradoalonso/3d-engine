#version 430 core

layout(vertices = 3) out;

uniform float tessLevelOuter;
uniform float tessLevelInner;

void main(void)
{
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

	gl_TessLevelOuter[0] = tessLevelOuter;
	gl_TessLevelOuter[1] = tessLevelOuter;
	gl_TessLevelOuter[2] = tessLevelOuter;


	gl_TessLevelInner[0] = tessLevelInner;
}