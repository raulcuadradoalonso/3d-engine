#version 430 core

layout (triangles) in;
layout (line_strip, max_vertices = 4) out;

in vec3 norm[3];

uniform mat4 projectionMatrix;

float offset = 0.01f;

out vec3 gColor;

void main() 
{
	gColor = vec3(0.0, 1.0, 0.0);

    gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(normalize(norm[0]) * offset, 0.0));
	EmitVertex();
	gl_Position = projectionMatrix * (gl_in[1].gl_Position + vec4(normalize(norm[1]) * offset, 0.0));
	EmitVertex();
	gl_Position = projectionMatrix * (gl_in[2].gl_Position + vec4(normalize(norm[2]) * offset, 0.0));
	EmitVertex();
	gl_Position = projectionMatrix * (gl_in[0].gl_Position + vec4(normalize(norm[0]) * offset, 0.0));
	EmitVertex();
	EndPrimitive();
} 