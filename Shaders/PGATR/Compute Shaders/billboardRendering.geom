#version 430 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

float quad_length = 50.0;

uniform mat4 modelView;
uniform mat4 projectionMatrix;

in vec4 vColor[];

out vec4 gColor;

void main() 
{    

	float halfLength = quad_length * 0.5f;
	gColor = vColor[0];

	vec4 center = modelView * gl_in[0].gl_Position;

	gl_Position = projectionMatrix * (center + vec4(-halfLength, halfLength, 0, 0));
    EmitVertex();	

	gl_Position = projectionMatrix * (center + vec4(-halfLength, -halfLength, 0, 0));
    EmitVertex();

	gl_Position = projectionMatrix * (center + vec4(halfLength, halfLength, 0, 0));
    EmitVertex();

	gl_Position = projectionMatrix * (center + vec4(halfLength, -halfLength, 0, 0));
    EmitVertex();        

	//Basic passthrough
	/*
	gColor = vColor[0];
	gl_Position = projectionMatrix * (modelView * gl_in[0].gl_Position);
	EmitVertex();
	*/
} 