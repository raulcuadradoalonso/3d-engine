#version 430 core

out vec4 outColor;

in vec4 vColor;

void main()
{
	outColor = vColor;
}
