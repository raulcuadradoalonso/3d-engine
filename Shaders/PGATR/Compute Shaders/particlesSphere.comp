#version 430 compatibility
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_storage_buffer_object : enable

struct pos
{
	vec4 pxyzw;	// positions
};

struct DefaultPos
{
	float x, y, z, w;
};

struct vel
{
	vec4 vxyzw;	// velocities
};

struct defaultVel
{
	float vx, vy, vz, vw;
};

struct color
{
	vec4 crgba;	// colors
};

struct defaultCol
{
	vec4 crgba;
};

struct defaultTime
{
	float t;
};

struct currentTime
{
	float t;
};

layout(std140, binding = 4) buffer Pos 
{
	pos  Positions[];
};

layout(std140, binding = 5) buffer Vel 
{
	vel  Velocities[];
};

layout(std140, binding = 6) buffer Col 
{
	color  Colors[];
};

layout(std140, binding = 7) buffer DefT 
{
	defaultTime  DefaultTimes[];
};

layout(std140, binding = 8) buffer CurrentT 
{
	currentTime  CurrentTimes[];
};

layout(std140, binding = 9) buffer DefaultEmissorPos 
{
	DefaultPos  DefaultPositions[];
};

layout(std140, binding = 10) buffer DefV 
{
	defaultVel  DefaultVelocities[];
};

layout(std140, binding = 11) buffer DefCol 
{
	defaultCol  DefaultColors[];
};

layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;


vec3 Bounce(vec3 vin, vec3 n)
{
	vec3 vout = reflect(vin, n);
	return vout;
}

vec3 BounceSphere(vec3 p, vec3 v, vec4 s)
{
	vec3 n = normalize(p - s.xyz);
	return Bounce(v, n);
}

bool IsInsideSphere(vec3 p, vec4 s)
{
	float r = length(p - s.xyz);
	return (r < s.w);
}

float map(float value, float min1, float max1, float min2, float max2) 
{
  return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

void main()
{
	uint gid = gl_GlobalInvocationID.x;
	const float DT = 0.03;

	CurrentTimes[gid].t -= DT;

	if (CurrentTimes[gid].t <= 0)
	{
		Positions[gid].pxyzw.x = DefaultPositions[gid].x;
		Positions[gid].pxyzw.y = DefaultPositions[gid].y;
		Positions[gid].pxyzw.z = DefaultPositions[gid].z;
		Positions[gid].pxyzw.w = 1.0f;

		Velocities[gid].vxyzw.x = DefaultVelocities[gid].vx;
		Velocities[gid].vxyzw.y = DefaultVelocities[gid].vy;
		Velocities[gid].vxyzw.z = DefaultVelocities[gid].vz;
		Velocities[gid].vxyzw.w = 0.0f;

		CurrentTimes[gid].t = DefaultTimes[gid].t;
	}

	const vec3 G = vec3(0., -9.8, 0.);
	
	const vec4 SPHERE = vec4(-100., -800., 0.,  600.);	

    vec3 p  = Positions[gid].pxyzw.xyz;
    vec3 v  = Velocities[gid].vxyzw.xyz;

	vec3 F = G;

	vec3 pp = p + v*DT + .5*DT*DT*F;
	vec3 vp = v + F*DT;

	if(IsInsideSphere(pp, SPHERE))
	{
		vp = BounceSphere(p, v, SPHERE);
		pp = p + vp*DT + .5*DT*DT*G;
	}
	
    Positions[gid].pxyzw = vec4(pp, 1.);
    Velocities[gid].vxyzw.xyz = vp;
	Colors[gid].crgba.b = map(CurrentTimes[gid].t, 0, DefaultTimes[gid].t, 0, DefaultColors[gid].crgba.b);
}