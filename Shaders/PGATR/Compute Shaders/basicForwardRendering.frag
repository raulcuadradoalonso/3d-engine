#version 430 core

out vec4 outColor;

in vec3 pos;
in vec3 norm;

//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
float alpha = 500.0;
vec3 N;

//Propiedades de la luz
vec3 Ia = vec3 (0.3);
vec3 Id = vec3 (0.5);
vec3 Is = vec3 (1.0);
vec3 light_pos = vec3 (0.0); 

vec3 shade();

void main()
{
	Ka = vec3(1.0, 0.0, 0.0);
	Kd = vec3(1.0, 0.0, 0.0); 
	Ks = vec3(1.0);

	N = normalize(norm);

	outColor = vec4(shade(), 1.0);
}

vec3 shade()
{
	vec3 c = vec3(0.0);
	c = Ia * Ka;

	vec3 L = normalize (light_pos - pos);
	vec3 diffuse = Id * Kd * dot (L,N);
	c += clamp(diffuse, 0.0, 1.0);
	
	vec3 V = normalize (-pos); 
	vec3 R = normalize (reflect (-L,N)); 
	float factor = max (dot (R,V), 0.01); 
	vec3 specular = Is*Ks*pow(factor,alpha); 
	c += clamp(specular, 0.0, 1.0); 

	return c;
}


