#version 430 core

in vec4 inPos;
in vec4 inColor;

out vec4 vColor;

uniform mat4 modelViewProj;

void main()
{	
	vColor = inColor;
	gl_Position =  modelViewProj * inPos;
}