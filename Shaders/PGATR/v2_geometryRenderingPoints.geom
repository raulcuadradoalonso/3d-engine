#version 430 core

layout (points) in;
layout (points, max_vertices = 1) out;

out vec3 gColor;

uniform mat4 projectionMatrix;

void main() 
{    
	gColor = vec3(0.0, 1.0, 0.0);

    gl_Position = projectionMatrix * gl_in[0].gl_Position;
	gl_PointSize = 5.0f;
    EmitVertex();
} 