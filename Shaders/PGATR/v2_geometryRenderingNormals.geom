#version 430 core

layout (triangles) in;
layout (line_strip, max_vertices = 1024) out;

in vec3 norm[3];

uniform mat4 projectionMatrix;

float norm_length = 1;

out vec3 gColor;

void main() 
{   
	vec4 p0 = projectionMatrix * gl_in[0].gl_Position;
	vec4 p1 = projectionMatrix * gl_in[1].gl_Position;
	vec4 p2 = projectionMatrix * gl_in[2].gl_Position;

	vec4 p0_norm = p0 + normalize(projectionMatrix * vec4(norm[0], 0.0));
	vec4 p1_norm = p1 + normalize(projectionMatrix * vec4(norm[1], 0.0));
	vec4 p2_norm = p2 + normalize(projectionMatrix * vec4(norm[2], 0.0));

	gColor = vec3(0.0, 1.0, 0.0);

	gl_Position = p0;
	EmitVertex();
	gl_Position = p0_norm;
	EmitVertex();
	EndPrimitive();

	gl_Position = p1;
	EmitVertex();
	gl_Position = p1_norm;
	EmitVertex();
	EndPrimitive();

	gl_Position = p2;
	EmitVertex();
	gl_Position = p2_norm;
	EmitVertex();
	EndPrimitive();

	gColor = vec3(0.0, 0.0, 1.0);

	vec4 center_point = (p0 + p1 + p2) / 3;
	vec4 center_normal = (p0_norm + p1_norm + p2_norm) / 3;

	gl_Position = center_point;
	EmitVertex();
	gl_Position = center_normal;
	EmitVertex();
	EndPrimitive();
} 