#pragma once
#include "Light.h"

class LightPoint : public Light
{
public:

	LightPoint(glm::vec3 pos, glm::vec3 diffuse, glm::vec3 specular, float range, float intensity);

	void MoveLight(glm::vec3 movement);

	//Getters	
	glm::vec3 GetPosition()const;	
	float GetRange() const;

	//Setters
	void SetPosition(glm::vec3 pos);
	void SetRange(float range);

private:

	//Light properties
	glm::vec3 m_position;
	float m_range;
};