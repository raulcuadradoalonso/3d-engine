#pragma once

//Describimos un plano en coordenadas normalizadas
//*************************************************

//N�mero de v�rtices
const int triangleNVertex = 3; // 4 v�rtices

//Posic�on de los vertices
/*const float triangleVertexPos[] = {
	//Cara z = 1
	-1.0f,	-1.0f,	 0.0f,
	 1.0f,	-1.0f,	 0.0f,
	 0.0f,	 1.0f,	 0.0f,
};*/

//Posic�on de los vertices
const float triangleVertexPos[] = {
	//Cara z = 1
	0.0f,	 6.0f,	 3.0f,
	-3.0f,	-1.0f,	 -6.0f,
	 0.0f,	-2.5f,	 2.0f,
};