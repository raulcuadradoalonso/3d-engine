#pragma once

class EngineDemo
{
public:
	virtual void CreateScene(int screen_width, int screen_height) = 0;
	virtual void RenderFunc() = 0;
	virtual void ResizeFunc(int screen_width, int screen_height) = 0;
	virtual void IdleFunc() = 0;
	virtual void KeyboardInput(unsigned char key, int x, int y) = 0;
	virtual void MouseFunc(int button, int state, int x, int y) = 0;
	virtual void MouseMotion(int x, int y) = 0;
};
