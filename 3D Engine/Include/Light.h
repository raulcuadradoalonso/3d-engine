#pragma once

class Program;

class Light
{
public:

	Light(glm::vec3 diffuse, glm::vec3 specular, float intensity);
	//Pure virtual destructor to force the class to be abstract
	virtual ~Light() = 0;

	//Getters
	glm::vec3 GetDiffuse() const;
	glm::vec3 GetSpecular() const;
	float GetIntensity() const;
	//Setters
	void SetDiffuse(glm::vec3 diffuse);	
	void SetSpecular(glm::vec3 specular);
	void SetIntensity(float intensity);

private:

	//Common features for every light
	float m_intensity;
	glm::vec3 m_diffuse;
	glm::vec3 m_specular;
};