#pragma once
#include "Program.h"
#include "Camera.h"

#define TOP	2147483647.	// 2^31 - 1	

struct Emisor
{
	float x, y, z, w;
};

class ParticleSystem
{
public:

	struct pos
	{
		float x, y, z, w;
	};	

	struct DefaultPos
	{
		float x, y, z, w;
	};

	struct vel
	{
		float vx, vy, vz, vw;
	};

	struct defaultVel
	{
		float vx, vy, vz, vw;
	};

	struct color
	{
		float r, g, b, a;
	};

	struct defaultCol
	{
		float r, g, b, a;
	};

	struct defaultTime
	{
		float t, a, b, c;
	};

	struct currentTime
	{
		float t, a, b, c;
	};	

	void initParticleSystem();
	void resetParticles(std::vector<Emisor> emisors);
	void drawParticles(const Camera &cam);
	void animateParticles();

	//Demo functions
	void setDefaultValues();
	void setExplosionValues();
	void setWaterValues();
	void setFireValues(std::vector<Emisor> emisors);
	void setAtractorsDemo();
	void setSphereDemo();

private:

	bool isAnimateParticlesActive;

	//Random parameters
	float XMIN = -1.0;
	float XMAX = 1.0;
	float YMIN = -1.0;
	float YMAX = 1.0;
	float ZMIN = -1.0;
	float ZMAX = 1.0;

	float VXMIN = -10.;
	float VXMAX = 10.;
	float VYMIN = -10.;
	float VYMAX = 10.;
	float VZMIN = -10.;
	float VZMAX = 10.;

	float MINTIME = 2.0f;
	float MAXTIME = 8.0f;

	const int NUM_PARTICLES = 600;
	const int WORK_GROUP_SIZE = 128;

	//Buffers IDs
	GLuint vao;
	GLuint posSSbo;
	GLuint velSSbo;
	GLuint colSSbo;
	GLuint defaultTimeSSbo;
	GLuint currentTimeSSbo;
	GLuint defaultEmissorPosSSbo;
	GLuint defaultVelSSbo;
	GLuint defaultColSSbo;

	//Compute shader
	std::unique_ptr<Program> csProgram;
	GLuint csProgramID;
	//Particles rendering
	std::unique_ptr<Program> renderingProgram;
	GLuint renderingProgramID;

	//-----Other functions
	float ranf(float low, float high);
};