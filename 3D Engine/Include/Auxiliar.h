#pragma once

//Loads a texture and returns a pointer to its location in main memory
//It also returns the size of the texture (w,h)
unsigned char *loadTexture(const char* fileName, unsigned int &w, unsigned int &h);

//Loads a file into a string
char *loadStringFromFile(const char *fileName, unsigned int &fileLen);