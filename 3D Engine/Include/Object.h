#pragma once

#include "Mesh.h"

class Camera;
class Material;
class GBuffer;

enum class OBJECTS
{
	TRIANGLE,
	QUAD,
	PLANE,
	CUBE,
	SPHERE,
	TEAPOT,
	CUSTOM
};

class Object
{
public:
	//Creation functions	
	static std::unique_ptr<Object> createObject(OBJECTS newObject, std::shared_ptr<Material> &mat, std::string path = "");
	Object(std::unique_ptr<Mesh> mh, std::shared_ptr<Material> &mat);
	Object(const Object &obj);
	Object& operator=(Object obj);

	//Transform functions
	void move(const glm::vec3 translation);
	void rotate(const float angle, const glm::vec3 axis);
	void scale(const glm::vec3 s);
	
	//Render functions		
	//Sometimes it is needed to render an object first with a different shader than the one it has attached
	//so if mat is not null, it is used to render the object instead of the currently attached one
	void renderObject(const Camera &cam, const std::shared_ptr<Material>& mat = nullptr, const bool drawPoints = false) const;
	//Render screen plane is used when you want to use a quad to add post-process effects
	void renderScreenQuad() const;
	void deferredShadingRender(const GBuffer &gbuffer);

	//Getters & setters
	Material& getMaterial() const;
	glm::mat4 getModel() const;
	Mesh& getMesh() const;
	void setMaterial(std::shared_ptr<Material> &m);
	std::shared_ptr<Material> swapMaterial(std::shared_ptr<Material> &m);

	//Tessellation properties
	void setTessLevelOuter(const float tlo);
	void setTessLevelInner(const float tli);

private:	

	void pushUniforms(const Camera &cam, const std::shared_ptr<Material>& mat, const bool renderToFBO) const;
	void drawObject(const bool drawPoints) const;
	void drawTessellatedObject()const;

	std::unique_ptr<Mesh> m_mesh;
	std::shared_ptr<Material> m_material;
	glm::mat4 m_model;

	float m_tessLevelOuter;
	float m_tessLevelInner;
};
