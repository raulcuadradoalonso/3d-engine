#pragma once
#include "EngineDemo.h"
#include "Camera.h"
#include "Scene.h"
#include <ctime>
#include "ParticleSystem.h"

class ParticlesDemo : public EngineDemo
{
public:	

	virtual void CreateScene(int screen_width, int screen_height) override;
	virtual void RenderFunc() override;
	virtual void ResizeFunc(int screen_width, int screen_height) override;
	virtual void IdleFunc() override;
	virtual void KeyboardInput(unsigned char key, int x, int y) override;
	virtual void MouseFunc(int button, int state, int x, int y) override;
	virtual void MouseMotion(int x, int y) override;

private:

	//-----mouse status
	bool mouse_left_button = false;

	//-----Rendering
	bool forwardRendering = true;
	bool deferredShading = false;
	bool geometryShaderSecondPass = false;

	//-----Camera settings
	glm::vec3 cam_position;
	glm::vec3 cam_forward;
	glm::vec3 cam_up;
	bool orbital_camera;
	float fov;
	float near_plane;
	float far_plane;
	float cam_speed;
	//C�mara orbital
	int mouse_x_ref;
	int mouse_y_ref;
	float theta;
	float phi;
	float camera_radius = glm::length(cam_position);
	//Camera zoom
	float camZoomSpeed;

	ParticleSystem m_particleSystem;

	//-----Scene
	std::unique_ptr<Scene> myScene;

	//-----Benchmark
	double animationTotalTime;
	double renderingTotalTime;
	double totalTime;

	unsigned int numberOfIterations = 1000;
	unsigned int currentIteration = 0;
};