#pragma once
#include "EngineDemo.h"
#include "Scene.h"

class SceneDemo : public EngineDemo
{
public:
	virtual void CreateScene(int screen_width, int screen_height) override;
	virtual void RenderFunc() override;
	virtual void ResizeFunc(int screen_width, int screen_height) override;
	virtual void IdleFunc() override;
	virtual void KeyboardInput(unsigned char key, int x, int y) override;
	virtual void MouseFunc(int button, int state, int x, int y) override;
	virtual void MouseMotion(int x, int y) override;

private:
	//-----mouse status
	bool mouse_left_button = false;

	//-----Camera settings
	glm::vec3 cam_position = glm::vec3(0, 0, 5);
	glm::vec3 cam_forward = glm::vec3(0, 0, -1);
	glm::vec3 cam_up = glm::vec3(0, 1, 0);
	bool orbital_camera = true;
	float fov = 60.0f;
	float near_plane = 0.1f;
	float far_plane = 3000.0f;
	float cam_speed = 0.1f;
	//Orbital camera
	int mouse_x_ref;
	int mouse_y_ref;
	float theta;
	float phi;
	float camera_radius = glm::length(cam_position);
	//Camera zoom
	float camZoomSpeed = 0.1f;

	//-----Scene
	std::unique_ptr<Scene> myScene;
	std::vector<std::shared_ptr<Object>> m_objects;
};