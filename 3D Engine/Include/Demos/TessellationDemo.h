#pragma once
#include "EngineDemo.h"
#include "Camera.h"
#include "Scene.h"

class TessellationDemo : public EngineDemo
{
public:

	virtual void CreateScene(int screen_width, int screen_height) override;
	virtual void RenderFunc() override;
	virtual void ResizeFunc(int screen_width, int screen_height) override;
	virtual void IdleFunc() override;
	virtual void KeyboardInput(unsigned char key, int x, int y) override;
	virtual void MouseFunc(int button, int state, int x, int y) override;
	virtual void MouseMotion(int x, int y) override;

private:

	//-----mouse status
	bool mouse_left_button = false;

	//-----Rendering
	bool forward_rendering;
	bool deferred_shading;
	bool geometry_shader_pass;
	bool tessellation_shader_pass;
	bool draw_points;

	//Rendering versions
	bool v0_triQuadDemo = true;
	bool v1_basicTeapot = false;
	bool v2_advancedTeapot = false;

	//-----Camera settings
	glm::vec3 cam_position = glm::vec3(0, 0, 10);
	glm::vec3 cam_forward = glm::vec3(0, 0, -1);
	glm::vec3 cam_up = glm::vec3(0, 1, 0);
	bool orbital_camera = true;
	float fov = 60.0f;
	float near_plane = 0.1f;
	float far_plane = 200.0f;
	float cam_speed = 0.1f;
	//C�mara orbital
	int mouse_x_ref;
	int mouse_y_ref;
	float theta;
	float phi;
	float camera_radius = glm::length(cam_position);
	//Camera zoom
	float camZoomSpeed = 0.5f;

	//-----Scene
	std::unique_ptr<Scene> myScene;

	//-----Demo
	MESHES currentObject;
	float tess_level_outer = 1.0f;
	float tess_level_inner = 1.0f;

	GeometryShader m_gs;

	//----Materials	
	std::shared_ptr<Material> basic_material; // Basic forward rendering using phong
	std::shared_ptr<Material> advanced_material; //Tessellation with mapamundi textures
	std::shared_ptr<Material> advanced_flat_material; //Flat with mapamundi textures
	std::shared_ptr<Material> advanced_geometry_material; //'Tessellation' made in geometry stage
	std::shared_ptr<Material> current_geometry_material;

	//----Objects
	std::shared_ptr<Object> m_triangle;
	std::shared_ptr<Object> m_quad;
	std::shared_ptr<Object> m_teapot;

	//Aux functions
	void initializeMaterials();
	void setGeometryShader(GeometryShader gs, bool isQuad = false);
	std::string getGeometryShaderPath(GeometryShader gs);
	void resetCameraPos();
};