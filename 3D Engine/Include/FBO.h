#pragma once

class Material;

class FBO
{
public:
	FBO();
	~FBO();

	void resizeFBO(unsigned int w, unsigned int h);

	void pushUniforms(const Material &mat) const;

	//Getters
	const GLuint getFboID() const;
	const GLuint getColorBuffID() const;
	const GLuint getDepthBuffID() const;
	const GLuint getVertexBuffID() const;

private:

	GLuint m_fbo;
	GLuint m_colorBufferID;
	GLuint m_depthBufferID;
	GLuint m_vertexBufferID;
};