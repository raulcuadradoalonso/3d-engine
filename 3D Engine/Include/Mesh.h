#pragma once

#define IN_POS 0
#define IN_COLOR 1
#define IN_NORMAL 2
#define IN_TEXCOORD 3
#define IN_TANGENTS 4

enum class MESHES
{
	TRIANGLE_MESH,
	QUAD_MESH,
	PLANE_MESH,
	CUBE_MESH,		
	SPHERE_MESH,
	TEAPOT_MESH,
	CUSTOM_MESH
};

class Mesh
{
public:

	static std::unique_ptr<Mesh> NewMesh(MESHES mesh, std::string path = "");

	Mesh(MESHES mesh, std::string path = "");
	~Mesh();

	GLuint getVAOindex();
	GLuint getNumFaces();
	GLuint getNumVertex();
	MESHES getTypeOfMesh();

private:

	void buildMesh(const GLint &numVertices,
		const GLint &numFaces,
		const float *vertexPositions,
		const float *vertexColors,
		const float *vertexNormals,
		const float *vertexTexCoords,
		const float *vertexTangents,
		const GLuint *facesIndices);

	void loadMeshFromAssimp(std::string file);

	MESHES m_typeOfMesh;

	GLuint vao = 0;
	GLuint posVBO = 0;
	GLuint colorVBO = 0;
	GLuint normalVBO = 0;
	GLuint texCoordVBO = 0;
	GLuint tangentVBO = 0;
	GLuint indexVBO = 0;

	GLuint m_numFaces;
	GLuint m_numVertices;
};
