#pragma once
#include "Program.h"

enum class MATERIALS
{
	STANDARD_FR,
	STANDARD_DR,
	CUSTOM
};

class Material
{
public:

	static std::unique_ptr<Material> CreateMaterial(MATERIALS mat_type);
	Material(std::shared_ptr<Program> p);
	~Material();

	void createColorTexture(const char* path);
	void createEmitTexture(const char* path);
	void createSpecularTexture(const char* path);
	void createNormalTexture(const char* path);
	//Getters & setters
	const Program& getProgram() const;
	GLuint getProgramID() const;
	GLuint getColorTextureBufferID() const;
	GLuint getEmiTextureBufferID() const;
	GLuint getSpecTextureBufferID() const;
	GLuint getNormalTextureBufferID() const;
	//Used in cases when we want to bind to the material an already created texture
	void setColorTextureBufferID(GLuint colorTexBufferID);

private:

	std::shared_ptr<Program> m_program;
	GLuint m_programID;
	GLuint m_colorTextureBufferID;
	GLuint m_emitTextureBufferID;
	GLuint m_specTextureBufferID;
	GLuint m_normalTextureBufferID;

	unsigned int loadTex(const char* fileName);

	void deleteColorTex();
	void deleteEmiTex();
	void deleteSpecTex();
	void deleteNormalTex();
};