#pragma once

class Material;
class Object;
class Camera;

struct DepthOfFieldConfig
{
	float focalDistance;
	float focusRange;
	float maxDistanceFactor;
};

class PostProcessingStack
{
public:

	PostProcessingStack();
	PostProcessingStack(const PostProcessingStack& pps);

	void PushPostProcessingUniforms(const Material& mat) const;

	void SetDepthOfFieldConfig(const DepthOfFieldConfig& dof);
	const DepthOfFieldConfig* GetDepthOfFieldConfig() const;

private:

	//Depth of field
	std::unique_ptr<DepthOfFieldConfig> m_dof;
};