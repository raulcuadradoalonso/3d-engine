#ifndef __QUADFILE__
#define __QUADFILE__

//Plane described in normalized coordinates
//*************************************************

const int quadNVertex = 4;

const float quadVertexPos[] = { 
	//Face z = 1
	-1.0f,	-1.0f,	 0.0f, 
	 1.0f,	-1.0f,	 0.0f, 
	-1.0f,	 1.0f,	 0.0f, 
	 1.0f,	 1.0f,	 0.0f, 
 };

const float quadVertexColors[] = {
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f
};

#endif