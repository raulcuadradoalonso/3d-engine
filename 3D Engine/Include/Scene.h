#pragma once
#include "Material.h"
#include "Camera.h"
#include "Object.h"
#include "gbuffer.h"
#include "LightPoint.h"
#include "FBO.h"
#include "PostProcessingStack.h"
#include "ParticleSystem.h"

enum class GeometryShader
{
	NORMALS,
	POINTS,
	WIREFRAME
};

enum class RenderType
{
	FORWARD_RENDERING,
	DEFERRED_RENDERING,
	GEOMETRY_RENDERING,
	TESSELLATION_RENDERING
};

class Scene
{
public:
	Scene();

	Scene(const int screen_width, const int screen_height, std::unique_ptr<Camera> cam, const bool deferredShading = false);
	
	void renderScene(const bool deferredShading = false, const GeometryShader gs = GeometryShader::WIREFRAME);
	void resizeScene(const int width, const int height, const float fov, const float n, const float f);
	void idleScene();
	void addObject(std::shared_ptr<Object> &o);
	void addPointLight(std::unique_ptr<LightPoint> lightPoint);
	void addParticleSystem(std::unique_ptr<ParticleSystem> pSystem);
	void removeObject(const size_t index);
	void setProjectionMatrixCamera(const float fv, const float asp, const float n, const float f);
	void moveOrbitalCamera(glm::vec3 pos, glm::vec3 fw, glm::vec3 u);
	void setTessLevelObject(const size_t index, const float tlo, const float tli);

	//Getters & setters
	Camera& getCam();
	std::vector<std::shared_ptr<Object>>& getObjects();
	std::vector<std::unique_ptr<ParticleSystem>>& getParticleSystems();


private:

	//-----VARIABLES-----
	//Security
	bool sceneInitialized;
	//Viewport settings
	int m_width;
	int m_height;
	//Render settings
	bool deferredShadingEnabled;
	//Camera
	std::unique_ptr<Camera> m_cam;
	//Objects in the scene
	std::vector<std::shared_ptr<Object>> m_objects;
	//Particle systems
	std::vector <std::unique_ptr<ParticleSystem>> m_particleSystems;
	//FBO for post-processing
	std::unique_ptr<FBO> m_fbo;

	//--------LIGHTS
	//Positional
	std::vector<std::unique_ptr<LightPoint>> m_pointLights;
	//Color of the ambiental light
	glm::vec3 m_ambient_color;
	//Material for the first pass only with ambient illumination
	std::vector<std::shared_ptr<Material>> m_ambientLightMaterials;

	//--------RENDERING
	//Material for deferred shading
	std::shared_ptr<Material> m_DeferredLightPassMat;
	//Deferred shading
	std::unique_ptr<Object> m_plane;
	GBuffer m_gBuffer;

	//-----FUNCTIONS-----
	void initScene();
	void renderSceneDSGeometryPass();
	void renderSceneDSLightPass();
	void renderSceneFR(const bool drawPoints = false);

	void renderAmbientLight();
	void pushPointLights(const LightPoint &light, const Program &program);
};