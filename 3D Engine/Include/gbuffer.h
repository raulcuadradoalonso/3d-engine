#ifndef GBUFFER_H
#define	GBUFFER_H

#include <GL/glew.h>
#include <stdio.h>
#include <cstring>

class GBuffer
{
public:
    
    GBuffer();

    ~GBuffer();

    bool Init();
	void ResizeGBuffer(unsigned int w, unsigned int h);
	void Bind();

	GLuint getPositionBufferID() const;
	GLuint getAlbedoSpecBufferID() const;
	GLuint getNormalBufferID() const;
	GLuint getDepthBufferID() const;

private:
                     
    GLuint m_fbo;
	GLuint m_position;
    GLuint m_albedo_spec_buffer;
	GLuint m_normals;
    GLuint m_depthTexture;
};

#endif

