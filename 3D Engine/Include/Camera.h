#pragma once

class Material;
class Object;
class PostProcessingStack;
class FBO;

class Camera
{
public:

	Camera();
	~Camera();

	glm::mat4 getView() const;
	glm::mat4 getProjection() const;
	void moveCamera(glm::vec3 translation);
	void rotateCamera(float angle, glm::vec3 axis);
	void setView(glm::vec3 pos, glm::vec3 fw, glm::vec3 u, bool isOrbital);
	void setProjection(float fv, float asp, float n, float f);
	void setPostProcessingStack(const PostProcessingStack &pps);

	bool hasPostProcessing() const;
	void postProcessingRender(const FBO &fbo) const;

	glm::vec3 getPosition() const;
	glm::vec3 getCenter() const;
	glm::vec3 getUp() const;
	glm::vec3 getForward() const;

	PostProcessingStack& getPostProcessingStack();	

private:

	bool viewInitialized;
	bool projectionInitialized;

	glm::mat4 m_View;
	glm::mat4 m_Projection;

	glm::vec3 m_position;
	glm::vec3 m_center;
	glm::vec3 m_up;

	std::unique_ptr<PostProcessingStack> m_postProcessingStack;
	//Material and plane for rendering the final image
	std::shared_ptr<Material> m_postProcessingMaterial;
	std::unique_ptr<Object> m_plane;
};