#pragma once

enum class SHADER_TYPE
{
	VERTEX,
	FRAGMENT,
	GEOMETRY,
	TESS_CTRL,
	TESS_EVAL,
	COMPUTE_SHADER
};

class Program
{
public:

	Program(const std::string vertexPath, const std::string fragmentPath, const std::string geometryPath = "",
		const std::string tessControlPath = "", const std::string tessEvaluationPath = "");
	Program(const std::string computeShaderPath);
	~Program();

	const GLuint getProgramID() const;

	//Matrices
	const GLint getModelViewID() const;
	const GLint getModelViewProjectionID() const;
	const GLint getNormalMatID() const;
	const GLint getProjectionMatID() const;
	const GLint getModelMatrixID() const;
	const GLint getViewMatrixID() const;
	//Lights
	const GLint getLightPosID() const;
	const GLint getLightDirID() const;
	const GLint getLightAmbientID() const;
	const GLint getLightDiffuseID() const;
	const GLint getLightSpecularID() const;
	const GLint getLightRangeID() const;
	const GLint getLightIntensityID() const;
	//Textures
	const GLint getColorTextureID() const;
	const GLint getEmitTextureID() const;
	const GLint getSpecTextureID() const;
	const GLint getNormalTextureID() const;
	//Deferred Shading
	const GLint getDSposID() const;
	const GLint getDScolorID() const;
	const GLint getDSnormalID() const;
	//Post-processing
	const GLint getColorFboTex() const;
	const GLint getVertexFboTex() const;
	const GLint getDepthFboTex() const;
	const GLint getFocalDistance() const;
	const GLint getMaxDistanceFactor() const;
	//Tessellation
	const GLint getTessLevelOuter() const;
	const GLint getTessLevelInner() const;
	//Status
	const GLint getRenderToFboID() const;

	const bool hasTessellation() const;

private:

	void initUniformIDs();

	GLuint programID;
	GLint m_vertexId = -1;
	GLint m_fragmentId = -1;
	GLint m_geometryId = -1; 
	GLint m_tessControlId = -1; 
	GLint m_tessEvaluationId = -1;
	GLint m_computShaderID = -1;
	
	//Matrices
	GLint uModelView = -1;
	GLint uModelViewProjection = -1;
	GLint uNormalMat = -1;
	GLint uProjectionMatrix = -1;
	GLint uModelMatrix = -1;
	GLint uView = -1;

	//Lights
	GLint uLightPosition = -1;
	GLint uLightDirection = -1;
	GLint uLightAmbient = -1;
	GLint uLightDiffuse = -1;
	GLint uLightSpecular = -1;
	GLint uLightRange = -1;
	GLint uLightIntensity = -1;

	//Textures
	GLint uColorTexture = -1;
	GLint uEmitTexture = -1;
	GLint uSpecTexture = -1;
	GLint uNormalTexture = -1;

	//Textures state
	GLint uHasColorTexture = -1;
	GLint uHasEmitTexture = -1;
	GLint uHaSpecTexture = -1;
	GLint uHasNormalTexture = -1;

	//Deferred Shading
	GLint uDSPositionTex = -1;
	GLint uDSColorTex = -1;
	GLint uDSNormalTex = -1;

	//Tessellation
	GLint tessLevelOuter = -1;
	GLint tessLevelInner = -1;

	//Post-processing
	GLint uColorFboTex = -1;
	GLint uVertexFboTex = -1;
	GLint uDepthFboTex = -1;

	GLint uFocalDistance = -1;
	GLint uMaxDistanceFactor = -1;

	//Status
	GLint uRenderToFBO = -1;
};