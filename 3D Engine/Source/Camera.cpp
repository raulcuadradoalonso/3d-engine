#include "stdafx.h"
#include "Program.h"
#include "Object.h"
#include "Material.h"
#include "PostProcessingStack.h"
#include "FBO.h"
#include "Camera.h"

Camera::Camera()
{
	viewInitialized = false;
	projectionInitialized = false;

	m_postProcessingStack = std::make_unique<PostProcessingStack>();
	m_postProcessingMaterial = std::make_shared<Material>(std::make_shared<Program>("../Shaders/PostProcessing/postProcessing.vert",
		"../Shaders/PostProcessing/postProcessing.frag"));

	//Quad for rendering the screen at the end (post-process)
	m_plane.reset(Object::createObject(OBJECTS::QUAD, m_postProcessingMaterial).release());
}

Camera::~Camera()
{

}

void Camera::setView(glm::vec3 pos, glm::vec3 fw, glm::vec3 u, bool isOrbital)
{
	m_position = pos;
	m_center = fw;
	m_up = u;

	glm::vec3 camera_center = (isOrbital) ? glm::vec3(0.0) : m_center - m_position;
	
	m_View = glm::lookAt(m_position, camera_center, m_up);

	viewInitialized = true;
}

void Camera::setProjection(float fv, float asp, float n, float f)
{
	m_Projection = glm::perspective(glm::radians(fv), asp, n, f);
	projectionInitialized = true;
}

void Camera::setPostProcessingStack(const PostProcessingStack &pps)
{
	m_postProcessingStack = std::make_unique<PostProcessingStack>(pps);
}

void Camera::rotateCamera(float angle, glm::vec3 axis)
{
	if (!viewInitialized)
	{
		std::cout << "ERROR: Camera view matrix not defined!" << std::endl;
	}

	m_View = glm::rotate(m_View, glm::radians(angle), axis);
}

void Camera::moveCamera(glm::vec3 translation)
{
	std::cout << "ERROR: Camera movement not implemented yet!" << std::endl;
}

bool Camera::hasPostProcessing() const
{
	return m_postProcessingStack->GetDepthOfFieldConfig();
}

void Camera::postProcessingRender(const FBO &fbo) const
{
	glUseProgram(m_postProcessingMaterial->getProgramID());

	//Push FBO uniforms
	fbo.pushUniforms(*m_postProcessingMaterial);

	//Push post-processing stack uniforms
	m_postProcessingStack->PushPostProcessingUniforms(*m_postProcessingMaterial);

	//Push camera uniforms (near and far planes)

	//Render quad on the screen
	glDisable(GL_DEPTH_TEST);
	m_plane->renderScreenQuad();
	glEnable(GL_DEPTH_TEST);
}

glm::mat4 Camera::getView() const
{
	if (!viewInitialized)
	{
		std::cout << "ERROR: Camera view matrix not defined!" << std::endl;
	}

	return m_View;
}

glm::mat4 Camera::getProjection() const
{
	if (!projectionInitialized)
	{
		std::cout << "ERROR: Camera projection matrix not defined!" << std::endl;
	}

	return m_Projection;
}

glm::vec3 Camera::getPosition() const
{
	return m_position;
}

glm::vec3 Camera::getCenter() const
{
	return m_center;
}

glm::vec3 Camera::getUp() const
{
	return m_up;
}

glm::vec3 Camera::getForward() const
{
	return glm::normalize(m_center - m_position);
}

PostProcessingStack& Camera::getPostProcessingStack()
{
	return *m_postProcessingStack;
}