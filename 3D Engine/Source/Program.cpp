#include "stdafx.h"
#include "Program.h"

Program::Program(const std::string vertexPath, const std::string fragmentPath, const std::string geometryPath,
	const std::string tessControlPath, const std::string tessEvaluationPath)
{
	//VERTEX SHADER
	GLuint fileLength;
	char *source = loadStringFromFile(vertexPath.c_str(), fileLength);
	m_vertexId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(m_vertexId, 1,
		(const GLchar **)&source, (const GLint *)&fileLength);
	glCompileShader(m_vertexId);
	delete[] source;

	//Check for successful compilation
	GLint compiled;
	glGetShaderiv(m_vertexId, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Log an error
		GLint logLen;
		glGetShaderiv(m_vertexId, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(m_vertexId, logLen, NULL, logString);
		std::cout << "Error in " << vertexPath << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(m_vertexId);
		//return (-1);
	}

	//GEOMETRY SHADER
	if (geometryPath != "")
	{
		source = loadStringFromFile(geometryPath.c_str(), fileLength);
		m_geometryId = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(m_geometryId, 1,
			(const GLchar **)&source, (const GLint *)&fileLength);
		glCompileShader(m_geometryId);
		delete[] source;

		//Check for successful compilation
		glGetShaderiv(m_geometryId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			//Calculamos una cadena de error
			GLint logLen;
			glGetShaderiv(m_geometryId, GL_INFO_LOG_LENGTH, &logLen);
			char *logString = new char[logLen];
			glGetShaderInfoLog(m_geometryId, logLen, NULL, logString);
			std::cout << "Error in " << fragmentPath << ": " << logString << std::endl;
			delete[] logString;
			glDeleteShader(m_geometryId);
			//return(-1);
		}
	}

	//TESSELATION CONTROL SHADER
	if (tessControlPath != "")
	{
		source = loadStringFromFile(tessControlPath.c_str(), fileLength);
		m_tessControlId = glCreateShader(GL_TESS_CONTROL_SHADER);
		glShaderSource(m_tessControlId, 1,
			(const GLchar **)&source, (const GLint *)&fileLength);
		glCompileShader(m_tessControlId);
		delete[] source;

		//Check for successful compilation
		glGetShaderiv(m_tessControlId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			//Calculamos una cadena de error
			GLint logLen;
			glGetShaderiv(m_tessControlId, GL_INFO_LOG_LENGTH, &logLen);
			char *logString = new char[logLen];
			glGetShaderInfoLog(m_tessControlId, logLen, NULL, logString);
			std::cout << "Error in " << fragmentPath << ": " << logString << std::endl;
			delete[] logString;
			glDeleteShader(m_tessControlId);
			//return(-1);
		}
	}

	//TESSELATION EVALUATION SHADER
	if (tessEvaluationPath != "")
	{
		source = loadStringFromFile(tessEvaluationPath.c_str(), fileLength);
		m_tessEvaluationId = glCreateShader(GL_TESS_EVALUATION_SHADER);
		glShaderSource(m_tessEvaluationId, 1,
			(const GLchar **)&source, (const GLint *)&fileLength);
		glCompileShader(m_tessEvaluationId);
		delete[] source;

		//Check for successful compilation
		glGetShaderiv(m_tessEvaluationId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			//Calculamos una cadena de error
			GLint logLen;
			glGetShaderiv(m_tessEvaluationId, GL_INFO_LOG_LENGTH, &logLen);
			char *logString = new char[logLen];
			glGetShaderInfoLog(m_tessEvaluationId, logLen, NULL, logString);
			std::cout << "Error in " << fragmentPath << ": " << logString << std::endl;
			delete[] logString;
			glDeleteShader(m_tessEvaluationId);
			//return(-1);
		}
	}

	//FRAGMENT SHADER
	source = loadStringFromFile(fragmentPath.c_str(), fileLength);
	m_fragmentId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_fragmentId, 1,
		(const GLchar **)&source, (const GLint *)&fileLength);
	glCompileShader(m_fragmentId);
	delete[] source;

	//Check for successful compilation
	glGetShaderiv(m_fragmentId, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(m_fragmentId, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(m_fragmentId, logLen, NULL, logString);
		std::cout << "Error in " << fragmentPath << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(m_fragmentId);
		//return(-1);
	}

	//Link program
	programID = glCreateProgram();
	glAttachShader(programID, m_vertexId);
	if (geometryPath != "") { glAttachShader(programID, m_geometryId); }
	if (tessControlPath != "") { glAttachShader(programID, m_tessControlId); }
	if (tessEvaluationPath != "") { glAttachShader(programID, m_tessEvaluationId); }
	glAttachShader(programID, m_fragmentId);
	glLinkProgram(programID);

	GLint linked;
	glGetProgramiv(programID, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(programID, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(programID);
		programID = 0;
		//return (-1);
	}

	initUniformIDs();
}

Program::Program(const std::string computeShaderPath)
{
	//COMPUTE SHADER
	GLuint fileLength;
	char *source = loadStringFromFile(computeShaderPath.c_str(), fileLength);
	m_computShaderID = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(m_computShaderID, 1,
		(const GLchar **)&source, (const GLint *)&fileLength);
	glCompileShader(m_computShaderID);
	delete[] source;

	//Check for successful compilation
	GLint compiled;
	glGetShaderiv(m_computShaderID, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(m_computShaderID, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(m_computShaderID, logLen, NULL, logString);
		std::cout << "Error in " << computeShaderPath << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(m_computShaderID);
		//return (-1);
	}

	programID = glCreateProgram();
	glAttachShader(programID, m_computShaderID);
	glLinkProgram(programID);

	GLint linked;
	glGetProgramiv(programID, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(programID, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(programID);
		programID = 0;
		//return (-1);
	}
}

void Program::initUniformIDs()
{
	//Matrices
	uModelView = glGetUniformLocation(programID, "modelView");
	uModelViewProjection = glGetUniformLocation(programID, "modelViewProj");
	uNormalMat = glGetUniformLocation(programID, "normal");
	uProjectionMatrix = glGetUniformLocation(programID, "projectionMatrix");
	uModelMatrix = glGetUniformLocation(programID, "modelMat");
	uView = glGetUniformLocation(programID, "view");

	//Textures
	uColorTexture = glGetUniformLocation(programID, "colorTex");
	uEmitTexture = glGetUniformLocation(programID, "emiTex");
	uSpecTexture = glGetUniformLocation(programID, "specularTex");
	uNormalTexture = glGetUniformLocation(programID, "normalTex");

	//Lights
	uLightPosition = glGetUniformLocation(programID, "lightPos");
	uLightDirection = glGetUniformLocation(programID, "lightDir");
	uLightAmbient = glGetUniformLocation(programID, "I_ambient");
	uLightDiffuse = glGetUniformLocation(programID, "I_diffuse");
	uLightSpecular = glGetUniformLocation(programID, "I_specular");	
	uLightRange = glGetUniformLocation(programID, "I_range");
	uLightIntensity = glGetUniformLocation(programID, "I_intensity");

	//Deferred shading
	uDSPositionTex = glGetUniformLocation(programID, "ds_positionTex");
	uDSColorTex = glGetUniformLocation(programID, "ds_colorTex");
	uDSNormalTex = glGetUniformLocation(programID, "ds_normalTex");

	//Tessellation
	tessLevelOuter = glGetUniformLocation(programID, "tessLevelOuter");
	tessLevelInner = glGetUniformLocation(programID, "tessLevelInner");

	//Post-processing
	uColorFboTex = glGetUniformLocation(programID, "colorTex");
	uVertexFboTex = glGetUniformLocation(programID, "vertexTex");
	uDepthFboTex = glGetUniformLocation(programID, "depthTex");

	uFocalDistance = glGetUniformLocation(programID, "focalDistance");
	uMaxDistanceFactor = glGetUniformLocation(programID, "maxDistanceFactor");

	//Status
	uRenderToFBO = glGetUniformLocation(programID, "renderToFBO");
}

Program::~Program()
{
	
	glDetachShader(programID, m_vertexId);
	glDetachShader(programID, m_fragmentId);

	glDeleteShader(m_vertexId);
	glDeleteShader(m_fragmentId);

	if (m_geometryId != -1)
	{
		glDetachShader(programID, m_geometryId);
		glDeleteShader(m_geometryId);
	}
	if (m_tessControlId != -1)
	{
		glDetachShader(programID, m_tessControlId);
		glDeleteShader(m_tessControlId);
	}
	if (m_tessEvaluationId != -1)
	{
		glDetachShader(programID, m_tessEvaluationId);
		glDeleteShader(m_tessEvaluationId);
	}

	glDeleteProgram(programID);
	
}

const GLuint Program::getProgramID() const
{
	return programID;
}

const GLint Program::getModelViewID() const
{
	return uModelView;
}

const GLint Program::getModelMatrixID() const
{
	return uModelMatrix;
}

const GLint Program::getModelViewProjectionID() const
{
	return uModelViewProjection;
}

const GLint Program::getNormalMatID() const
{
	return uNormalMat;
}

const GLint Program::getViewMatrixID() const
{
	return uView;
}

const GLint Program::getLightPosID() const
{
	return uLightPosition;
}

const GLint Program::getLightDirID() const
{
	return uLightDirection;
}

const GLint Program::getLightAmbientID() const
{
	return uLightAmbient;
}

const GLint Program::getLightDiffuseID() const
{
	return uLightDiffuse;
}

const GLint Program::getLightSpecularID() const
{
	return uLightSpecular;
}

const GLint Program::getLightRangeID() const
{
	return uLightRange;
}

const GLint Program::getLightIntensityID() const
{
	return uLightIntensity;
}

const GLint Program::getColorTextureID() const
{
	return uColorTexture;
}

const GLint Program::getEmitTextureID() const
{
	return uEmitTexture;
}

const GLint Program::getSpecTextureID() const
{
	return uSpecTexture;
}

const GLint Program::getNormalTextureID() const
{
	return uNormalTexture;
}

const GLint Program::getDSposID() const
{
	return uDSPositionTex;
}

const GLint Program::getDScolorID() const
{
	return uDSColorTex;
}

const GLint Program::getDSnormalID() const
{
	return uDSNormalTex;
}

const GLint Program::getProjectionMatID() const
{
	return uProjectionMatrix;
}

const GLint Program::getColorFboTex() const
{
	return uColorFboTex;
}

const GLint Program::getVertexFboTex() const
{
	return uVertexFboTex;
}

const GLint Program::getDepthFboTex() const
{
	return uDepthFboTex;
}

const GLint Program::getFocalDistance() const
{
	return uFocalDistance;
}

const GLint Program::getMaxDistanceFactor() const
{
	return uMaxDistanceFactor;
}

const GLint Program::getTessLevelInner() const
{
	return tessLevelInner;
}

const GLint Program::getTessLevelOuter() const
{
	return tessLevelOuter;
}

const GLint Program::getRenderToFboID() const
{
	return uRenderToFBO;
}

const bool Program::hasTessellation() const
{
	return (m_tessControlId != -1 && m_tessEvaluationId != -1);
}