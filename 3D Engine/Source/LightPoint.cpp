#include "stdafx.h"
#include "LightPoint.h"

LightPoint::LightPoint(glm::vec3 pos, glm::vec3 diffuse, glm::vec3 specular, float range, float intensity) :
	m_position(pos), m_range(range), Light(diffuse, specular, intensity)
{

}

void LightPoint::MoveLight(glm::vec3 movement)
{
	m_position += movement;
}

glm::vec3 LightPoint::GetPosition() const
{
	return m_position;
}

float LightPoint::GetRange() const
{
	return m_range;
}

void LightPoint::SetPosition(glm::vec3 position)
{
	m_position = position;
}

void LightPoint::SetRange(float range)
{
	m_range = range;
}