#include "stdafx.h"
#include "BOX.h"
#include "QUAD.h"
#include "TRIANGLE.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Mesh.h"

std::unique_ptr<Mesh> Mesh::NewMesh(MESHES mesh, std::string path)
{
	return std::make_unique<Mesh>(mesh, path);
}

Mesh::Mesh(MESHES mesh, std::string path)
{
	m_typeOfMesh = mesh;

	switch (mesh)
	{
	case MESHES::TRIANGLE_MESH:		
		buildMesh(triangleNVertex, 1, triangleVertexPos, nullptr, nullptr, nullptr, nullptr, nullptr);
		break;
	case MESHES::QUAD_MESH:
		buildMesh(quadNVertex, 1, quadVertexPos, quadVertexColors, nullptr, nullptr, nullptr, nullptr);
		break;
	case MESHES::PLANE_MESH:
		loadMeshFromAssimp("../Models/plane.FBX");
		break;
	case MESHES::CUBE_MESH:
		loadMeshFromAssimp("../Models/cube.fbx");
		break;	
	case MESHES::SPHERE_MESH:
		loadMeshFromAssimp("../Models/sphere.obj");
		break;
	case MESHES::TEAPOT_MESH:
		loadMeshFromAssimp("../Models/teapot.obj");
		break;
	case MESHES::CUSTOM_MESH:
		loadMeshFromAssimp(path);
		break;
	default:
		break;
	}
}

Mesh::~Mesh()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &posVBO);
	glDeleteBuffers(1, &colorVBO);
	glDeleteBuffers(1, &normalVBO);
	glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &indexVBO);

	glDeleteVertexArrays(1, &vao);
}

void Mesh::buildMesh(const GLint &numVertices, const GLint &numTriangles,
	const float *vertexPositions, const float *vertexColors,
	const float *vertexNormals, const float *vertexTexCoords,
	const float *vertexTangents, const GLuint *triangleIndices)
{
	m_numFaces = numTriangles;
	m_numVertices = numVertices;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &posVBO);
	glBindBuffer(GL_ARRAY_BUFFER, posVBO);
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float) * 3,
		vertexPositions, GL_STATIC_DRAW);
	glVertexAttribPointer(IN_POS, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(IN_POS);

	if (vertexColors != nullptr)
	{
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float) * 3,
			vertexColors, GL_STATIC_DRAW);
		glVertexAttribPointer(IN_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(IN_COLOR);
	}
	
	if (vertexNormals != nullptr)
	{
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float) * 3,
			vertexNormals, GL_STATIC_DRAW);
		glVertexAttribPointer(IN_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(IN_NORMAL);
	}
	
	if (vertexTexCoords != nullptr)
	{
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float) * 2,
			vertexTexCoords, GL_STATIC_DRAW);
		glVertexAttribPointer(IN_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(IN_TEXCOORD);
	}	

	if (vertexTangents != nullptr)
	{
		glGenBuffers(1, &tangentVBO);
		glBindBuffer(GL_ARRAY_BUFFER, tangentVBO);
		glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float) * 3,
			vertexTangents, GL_STATIC_DRAW);
		glVertexAttribPointer(IN_TANGENTS, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(IN_TANGENTS);
	}

	if (triangleIndices != nullptr)
	{
		glGenBuffers(1, &indexVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			numTriangles * sizeof(unsigned int) * 3, triangleIndices,
			GL_STATIC_DRAW);
	}	

	glBindVertexArray(0);
}

void Mesh::loadMeshFromAssimp(std::string file)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(file,
		//aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
		//aiProcess_GenNormals);

	if (!scene)
	{
		std::cout << importer.GetErrorString();
		exit(-1);
	}

	const aiMesh* mesh = scene->mMeshes[0];

	//Vertex positions
	aiVector3D* vertex = mesh->mVertices;
	std::vector<float> vertices;
	for (size_t i = 0; i < mesh->mNumVertices; i++)
	{
		vertices.push_back(vertex[i].x);
		vertices.push_back(vertex[i].y);
		vertices.push_back(vertex[i].z);
	}

	//Vertex triangles
	aiFace* original_face = mesh->mFaces;
	std::vector<unsigned int> faces;
	for (size_t i = 0; i < mesh->mNumFaces; i++)
	{
		faces.push_back(original_face[i].mIndices[0]);
		faces.push_back(original_face[i].mIndices[1]);
		faces.push_back(original_face[i].mIndices[2]);
	}

	//Vertex normals
	std::vector<float> normals;	
	if (mesh->HasNormals())
	{
		aiVector3D* original_normal = mesh->mNormals;	
		for (size_t i = 0; i < mesh->mNumVertices; i++)
		{
			normals.push_back(original_normal[i].x);
			normals.push_back(original_normal[i].y);
			normals.push_back(original_normal[i].z);
		}
	}
	const float *normalsPointer = (normals.size() != 0) ? normals.data() : nullptr;

	//Tangents
	std::vector<float> tangents;
	if (mesh->HasTangentsAndBitangents())
	{
		aiVector3D* original_tangents = mesh->mTangents;
		for (size_t i = 0; i < mesh->mNumVertices; i++)
		{
			tangents.push_back(original_tangents[i].x);
			tangents.push_back(original_tangents[i].y);
			tangents.push_back(original_tangents[i].z);
		}
	}

	//Vertex texture coordinates
	std::vector<float> texCoords;
	if (mesh->HasTextureCoords(0))
	{
		aiVector3D* original_texCoords = mesh->mTextureCoords[0];
		for (size_t i = 0; i < mesh->mNumVertices; i++)
		{
			texCoords.push_back(original_texCoords[i].x);
			texCoords.push_back(original_texCoords[i].y);
		}
	}
	const float *texCoordsPointer = (texCoords.size() != 0) ? texCoords.data() : nullptr;

	buildMesh(mesh->mNumVertices, mesh->mNumFaces, vertices.data(), nullptr, 
		       normalsPointer, texCoordsPointer,  tangents.data(), faces.data());
}

GLuint Mesh::getVAOindex()
{
	return vao;
}

GLuint Mesh::getNumFaces()
{
	return m_numFaces;
}

GLuint Mesh::getNumVertex()
{
	return m_numVertices;
}

MESHES Mesh::getTypeOfMesh()
{
	return m_typeOfMesh;
}