#include "stdafx.h"
#include "Material.h"

std::unique_ptr<Material> Material::CreateMaterial(MATERIALS mat_type)
{
	switch (mat_type)
	{
	case MATERIALS::STANDARD_FR:
		return std::make_unique<Material>(std::make_unique<Program>("../Shaders/Standard/fr_standard.vert",
			"../Shaders/Standard/fr_standard.frag"));
		break;
	case MATERIALS::STANDARD_DR:
		return std::make_unique<Material>(std::make_unique<Program>("../Shaders/Standard/dr_standard.vert",
			"../Shaders/Standard/dr_standard.frag"));
		break;
	}
}

Material::Material(std::shared_ptr<Program> p)
	: m_program(p)
{
	m_colorTextureBufferID = 0;
	m_emitTextureBufferID = 0;
	m_specTextureBufferID = 0;
	m_normalTextureBufferID = 0;
	m_programID = m_program->getProgramID();
}

Material::~Material()
{
	deleteColorTex();
	deleteEmiTex();
	deleteSpecTex();
	deleteNormalTex();
}

void Material::createColorTexture(const char *path)
{
	if (m_colorTextureBufferID != 0) { deleteColorTex(); }

	m_colorTextureBufferID = loadTex(path);
}

void Material::createEmitTexture(const char *path)
{
	if (m_emitTextureBufferID != 0) { deleteEmiTex(); }

	m_emitTextureBufferID = loadTex(path);
}

void Material::createSpecularTexture(const char *path)
{
	if (m_specTextureBufferID != 0) { deleteSpecTex(); }

	m_specTextureBufferID = loadTex(path);
}

void Material::createNormalTexture(const char* path)
{
	if (m_normalTextureBufferID != 0) { deleteNormalTex(); }

	m_normalTextureBufferID = loadTex(path);
}

unsigned Material::loadTex(const char *fileName)
{
	if (fileName == NULL) return NULL;

	unsigned char *map;
	unsigned int w, h;
	map = loadTexture(fileName, w, h);

	if (!map)
	{
		std::cout << "Error cargando el fichero: "
			<< fileName << std::endl;
		exit(-1);
	}

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, (GLvoid*)map);
	delete[] map;
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	GLfloat fLargest;

	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);

	// Anisotropic filter
	if (fLargest != -1)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
	}

	return texId;
}

void Material::deleteColorTex()
{
	glDeleteTextures(1, &m_colorTextureBufferID);
}

void Material::deleteEmiTex()
{
	glDeleteTextures(1, &m_emitTextureBufferID);
}

void Material::deleteSpecTex()
{
	glDeleteTextures(1, &m_specTextureBufferID);
}

void Material::deleteNormalTex()
{
	glDeleteTextures(1, &m_normalTextureBufferID);
}

const Program& Material::getProgram() const
{
	return *m_program;
}

GLuint Material::getProgramID() const
{
	return m_programID;
}

GLuint Material::getColorTextureBufferID() const
{
	return m_colorTextureBufferID;
}

GLuint Material::getEmiTextureBufferID() const
{
	return m_emitTextureBufferID;
}

GLuint Material::getSpecTextureBufferID() const
{
	return m_specTextureBufferID;
}

GLuint Material::getNormalTextureBufferID() const
{
	return m_normalTextureBufferID;
}

void Material::setColorTextureBufferID(GLuint colorTexBufferID)
{
	if (m_colorTextureBufferID != 0) { deleteColorTex(); }

	m_colorTextureBufferID = colorTexBufferID;
}