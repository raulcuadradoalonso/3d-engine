#include "stdafx.h"
#include "Scene.h"

Scene::Scene()
{
	sceneInitialized = false;
}

Scene::Scene(const int screen_width, const int screen_height, std::unique_ptr<Camera> cam, const bool deferredShading) :
	m_width(screen_width), m_height(screen_height), m_cam(std::move(cam)), deferredShadingEnabled(deferredShading)
{	
	sceneInitialized = true;

	m_width = screen_width;
	m_height = screen_height;

	//Default color of ambient light
	m_ambient_color = glm::vec3(0.1f, 0.1f, 0.1f);	

	initScene();
}

void Scene::initScene()
{
	m_fbo = std::make_unique<FBO>();
	m_fbo->resizeFBO(m_width, m_height);

	if (deferredShadingEnabled)
	{
		//Init GBuffer
		m_gBuffer.Init();
		m_gBuffer.ResizeGBuffer(m_width, m_height);

		//Create deferred program
		std::unique_ptr<Program> program_ds_light_pass = std::make_unique<Program>("../Shaders/DSLightPass.vert",
																	"../Shaders/DSLightPass.frag");

		//Create material
		m_DeferredLightPassMat = std::make_shared<Material>(std::move(program_ds_light_pass));

		//Create an object with the material attached
		m_plane.reset(Object::createObject(OBJECTS::QUAD, m_DeferredLightPassMat).release());
	}
}

void Scene::idleScene()
{
	glutPostRedisplay();
}

void Scene::renderScene(const bool deferredShading, const GeometryShader gs)
{
	if (deferredShading)
	{
		renderSceneDSGeometryPass();
		renderSceneDSLightPass();
	}
	else
	{
		bool drawPoints = (gs == GeometryShader::POINTS) ? true : false;
		renderSceneFR(drawPoints);
	}	
}

void Scene::renderSceneDSGeometryPass() 
{
	//Initial settings
	m_gBuffer.Bind();
	glDepthMask(GL_TRUE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	size_t numOfObjects = m_objects.size();
	for (size_t i = 0; i < numOfObjects; i++)
	{
		m_objects[i]->renderObject(*m_cam);
	}

	//Restore settings
	glBindVertexArray(0);
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}

void Scene::renderSceneDSLightPass()
{	
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT);

	m_plane->deferredShadingRender(m_gBuffer);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}

void Scene::renderSceneFR(const bool drawPoints)
{
	//If post-process is activated, we render everything to an FBO
	if (m_cam->hasPostProcessing())
	{
		if (m_fbo) { glBindFramebuffer(GL_FRAMEBUFFER, m_fbo->getFboID()); }
		else { std::cout << "FBO not created!" << std::endl;  }
	}
	else { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//1.- AMBIENT LIGHT
	renderAmbientLight();
	
	//2.- POINT LIGHTS
	//----Blending for lights----
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDepthFunc(GL_EQUAL);

	for (size_t i = 0; i < m_objects.size(); i++)
	{
		//Currently each object is rendered 1 time for each light
		for (size_t j = 0; j < m_pointLights.size(); j++)
		{
			//Push light uniforms
			pushPointLights(*m_pointLights[j], m_objects[i]->getMaterial().getProgram());
			//Push object uniforms and render
			m_objects[i]->renderObject(*m_cam, nullptr, drawPoints);
		}		
	}	

	//3.- DIRECTIONAL LIGHTS

	//4.- FOCAL LIGHTS

	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	//----End blending for lights----

	//PARTICLE SYSTEMS
	for (size_t i = 0; i < m_particleSystems.size(); i++)
	{
		m_particleSystems[i]->animateParticles();
		m_particleSystems[i]->drawParticles(*m_cam);
	}

	//6.- POST PROCESS
	if (m_cam->hasPostProcessing())
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		m_cam->postProcessingRender(*m_fbo);
	}
}

void Scene::renderAmbientLight()
{
	for (size_t i = 0; i < m_objects.size(); i++)
	{
		glUseProgram(m_ambientLightMaterials[i]->getProgramID());

		//Push ambient light uniform
		glUniform3fv(m_ambientLightMaterials[i]->getProgram().getLightAmbientID(), 1, glm::value_ptr(m_ambient_color));

		m_objects[i]->renderObject(*m_cam, m_ambientLightMaterials[i]);
	}
}

void Scene::pushPointLights(const LightPoint &light, const Program &program)
{
	glUseProgram(program.getProgramID());

	//Position
	glUniform3fv(program.getLightPosID(), 1, glm::value_ptr(light.GetPosition()));
	//Diffuse
	glUniform3fv(program.getLightDiffuseID(), 1, glm::value_ptr(light.GetDiffuse()));
	//Specular
	glUniform3fv(program.getLightSpecularID(), 1, glm::value_ptr(light.GetSpecular()));	
	//Range
	glUniform1f(program.getLightRangeID(), light.GetRange());
	//Intensity
	glUniform1f(program.getLightIntensityID(), light.GetIntensity());
}

void Scene::resizeScene(const int width, const int height, const float fov, const float n, const float f)
{
	m_width = width;
	m_height = height;

	glViewport(0, 0, width, height);

	m_cam->setProjection(fov, (float)width / (float)height, n, f);

	m_gBuffer.ResizeGBuffer(width, height);

	m_fbo->resizeFBO(width, height);

	glutPostRedisplay();
}

//When an object is added, a new material for ambient lighting is created using its diffuse texture
void Scene::addObject(std::shared_ptr<Object> &o)
{
	m_objects.emplace_back(o);

	std::shared_ptr<Material> objectAmbientMaterial = std::make_shared<Material>(std::make_shared<Program>("../Shaders/Standard/fr_standard_ambient.vert",
		"../Shaders/Standard/fr_standard_ambient.frag"));
	objectAmbientMaterial->setColorTextureBufferID(o->getMaterial().getColorTextureBufferID());
	m_ambientLightMaterials.emplace_back(objectAmbientMaterial);
}

void Scene::addPointLight(std::unique_ptr<LightPoint> lightPoint)
{
	m_pointLights.emplace_back(lightPoint.release());
}

void Scene::addParticleSystem(std::unique_ptr<ParticleSystem> pSystem)
{
	m_particleSystems.emplace_back(pSystem.release());
}

void Scene::removeObject(const size_t index)
{
	m_objects.erase(m_objects.begin() + index);
	m_ambientLightMaterials.erase(m_ambientLightMaterials.begin() + index);
}

void Scene::setProjectionMatrixCamera(const float fv, const float asp, const float n, const float f)
{
	m_cam->setProjection(fv, asp, n, f);
}

void Scene::moveOrbitalCamera(glm::vec3 pos, glm::vec3 fw, glm::vec3 u)
{
	m_cam->setView(pos, fw, u, true);
}

void Scene::setTessLevelObject(const size_t index, const float tlo, const float tli)
{
	m_objects[index]->setTessLevelOuter(tlo);
	m_objects[index]->setTessLevelInner(tli);
}

Camera& Scene::getCam()
{
	return *m_cam;
}

std::vector<std::shared_ptr<Object>>& Scene::getObjects()
{
	return m_objects;
}

std::vector<std::unique_ptr<ParticleSystem>>& Scene::getParticleSystems()
{
	return m_particleSystems;
}