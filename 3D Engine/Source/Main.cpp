// OpenGL3DEngine.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Demos/DeferredShadingDemo.h"
#include "Demos/ParticlesDemo.h"
#include "Demos/TessellationDemo.h"
#include "Demos/SceneDemo.h"

#define WINDOW_WIDTH  500  
#define WINDOW_HEIGHT 500
#define PI 3.14159265359

//It can be of type: SceneDemo, DeferredShadingDemo, ParticlesDemo or TessellationDemo
std::unique_ptr<EngineDemo> currentDemo = std::make_unique<SceneDemo>();

void initContext(int argc, char** argv);
void initOpenGL();
void createScene();
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotion(int x, int y);

int main(int argc, char **argv)
{
	initContext(argc, argv);
	initOpenGL();
	createScene();
	glutMainLoop();
}

void initContext(int argc, char** argv)
{
	//iniciar contexto
	std::locale::global(std::locale("spanish"));
	glutInit(&argc, argv);
	glutInitContextVersion(4, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("OpenGL 3D Render Engine");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}
	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotion);
}

void initOpenGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void createScene()
{
	currentDemo->CreateScene(WINDOW_WIDTH, WINDOW_HEIGHT);
}

void renderFunc()
{
	currentDemo->RenderFunc();
}

void resizeFunc(int width, int height)
{
	currentDemo->ResizeFunc(width, height);
}

void idleFunc()
{
	currentDemo->IdleFunc();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	currentDemo->KeyboardInput(key, x, y);
}

void mouseFunc(int button, int state, int x, int y)
{
	currentDemo->MouseFunc(button, state, x, y);
}

void mouseMotion(int x, int y)
{
	currentDemo->MouseMotion(x, y);
}