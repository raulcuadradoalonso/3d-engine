#include "stdafx.h" 
#include "Demos/ParticlesDemo.h"

void ParticlesDemo::CreateScene(int screen_width, int screen_height)
{
	std::cout << "Setting up particles demo..." << std::endl;

	//Init particles system
	m_particleSystem.initParticleSystem();
	m_particleSystem.setDefaultValues(); //This also initializes the buffers

	//Cam values
	cam_position = glm::vec3(0, 0, 1000);
	cam_forward = glm::vec3(0, 0, -1);
	cam_up = glm::vec3(0, 1, 0);
	orbital_camera = true;
	fov = 60.0f;
	near_plane = 0.1f;
	far_plane = 3000.0f;
	camZoomSpeed = 30.0f;

	//Init scenes	
	std::unique_ptr<Camera> myCam (new Camera());
	myCam->setView(cam_position, cam_forward, cam_up, orbital_camera);
	myCam->setProjection(fov, (float)screen_width / (float)screen_height, near_plane, far_plane);
	myScene = std::make_unique<Scene>(screen_width, screen_height, std::move(myCam));
}

void ParticlesDemo::RenderFunc()
{
	m_particleSystem.animateParticles();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_particleSystem.drawParticles(myScene->getCam());

	//myScene->renderScene();

	glutSwapBuffers();
}

void ParticlesDemo::ResizeFunc(int screen_width, int screen_height)
{
	myScene->resizeScene(screen_width, screen_height, fov, near_plane, far_plane);
}

void ParticlesDemo::IdleFunc()
{
	myScene->idleScene();
}

void ParticlesDemo::KeyboardInput(unsigned char key, int x, int y)
{
	//----- Camera controls
	if (key == '+')
	{
		cam_position = cam_position + (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}
	else if (key == '-')
	{
		cam_position = cam_position - (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}

	//----- Demo controls
	if (key == 'e')
	{
		m_particleSystem.setExplosionValues();
	}
	else if (key == 'w')
	{
		m_particleSystem.setWaterValues();
	}
	else if (key == 'f')
	{
		//m_particleSystem.setFireValues();
	}
	else if (key == 'd')
	{
		m_particleSystem.setDefaultValues();
	}
	else if (key == 'a')
	{
		m_particleSystem.setAtractorsDemo();
	}
	else if (key == 's')
	{
		m_particleSystem.setSphereDemo();
	}
}

void ParticlesDemo::MouseFunc(int button, int state, int x, int y)
{
	if (!mouse_left_button && button == GLUT_LEFT_BUTTON)
	{
		mouse_x_ref = x;
		mouse_y_ref = y;
		mouse_left_button = true;
	}
	else if (button == GLUT_LEFT_BUTTON && mouse_left_button)
	{
		mouse_left_button = false;
	}
}

void ParticlesDemo::MouseMotion(int x, int y)
{
	//Orbital camera
	if (mouse_left_button)
	{
		camera_radius = glm::length(cam_position);

		int x_offset = (x - mouse_x_ref);
		int y_offset = (y - mouse_y_ref);

		theta -= glm::radians((float)x_offset);
		phi += glm::radians((float)y_offset);

		if (phi > 1.5f)
		{
			phi = 1.5f;
		}
		else if (phi < -1.5f)
		{
			phi = -1.5f;
		}

		float eye_x = camera_radius * cos(phi) * sin(theta);
		float eye_y = camera_radius * sin(phi);
		float eye_z = camera_radius * cos(phi) * cos(theta);

		myScene->moveOrbitalCamera(glm::vec3(eye_x, eye_y, eye_z), glm::vec3(0, 0, 0), cam_up);

		cam_position = glm::vec3(eye_x, eye_y, eye_z);

		mouse_x_ref = x;
		mouse_y_ref = y;
	}
}