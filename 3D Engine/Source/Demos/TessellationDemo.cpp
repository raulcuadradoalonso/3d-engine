#include "stdafx.h"
#include "Demos/TessellationDemo.h"

void TessellationDemo::CreateScene(int screen_width, int screen_height)
{
	std::cout << "Setting up geometry rendering..." << std::endl;

	//Init general settings
	forward_rendering = true;
	deferred_shading = false;
	geometry_shader_pass = true;
	tessellation_shader_pass = true;
	draw_points = false;

	//Init scene
	std::unique_ptr<Camera> myCam (new Camera());
	myCam->setView(cam_position, cam_forward, cam_up, orbital_camera);
	myCam->setProjection(fov, (float)screen_width / (float)screen_height, near_plane, far_plane);
	myScene = std::make_unique<Scene>(screen_width, screen_height, std::move(myCam));

	m_gs = GeometryShader::WIREFRAME;
	setGeometryShader(m_gs);

	//----Create materials
	initializeMaterials();

	//------Create objects
	m_triangle = Object::createObject(OBJECTS::TRIANGLE, basic_material);
	m_quad = Object::createObject(OBJECTS::QUAD, basic_material);
	m_teapot = Object::createObject(OBJECTS::TEAPOT, basic_material);
	m_teapot->scale(glm::vec3(0.1f, 0.1f, 0.1f));

	//Initial scene setup:
	myScene->addObject(m_triangle);
	currentObject = MESHES::TRIANGLE_MESH;
}

void TessellationDemo::RenderFunc()
{
	if (forward_rendering)
	{
		//Forward rendering pass
		myScene->renderScene();
	}
	
	if (geometry_shader_pass)
	{		
		//Pass with geometry shaders
		std::vector<std::shared_ptr<Object>>& objects = myScene->getObjects();
		std::vector<std::shared_ptr<Material>> originalMaterials;
		originalMaterials.resize(objects.size());		

		//Store current materials to apply the one with geometry shader enabled		
		for (size_t i = 0; i < objects.size(); i++)
		{			
			originalMaterials[i] = objects[i]->swapMaterial(current_geometry_material);
		}
		
		glEnable(GL_PROGRAM_POINT_SIZE);

		myScene->renderScene(false, m_gs);

		glDisable(GL_PROGRAM_POINT_SIZE);

		
		//Restore the materials
		for (size_t i = 0; i < objects.size(); i++)
		{
			objects[i]->swapMaterial(originalMaterials[i]);
		}		
	}	
	
	glutSwapBuffers();
}

void TessellationDemo::ResizeFunc(int screen_width, int screen_height)
{
	myScene->resizeScene(screen_width, screen_height, fov, near_plane, far_plane);
}

void TessellationDemo::IdleFunc()
{
	myScene->idleScene();
}

void TessellationDemo::KeyboardInput(unsigned char key, int x, int y)
{
	//-----Render controls	
	//Features only available in basic version
	if (v0_triQuadDemo)
	{
		if (currentObject != MESHES::TRIANGLE_MESH && key == 't')
		{
			currentObject = MESHES::TRIANGLE_MESH;

			//Call this to load the geometry shader adapted to triangles again
			//because there is a second parameter, isQuad, which is set to false by default.
			//When this value is false, the setGeometryShader function loads a shader
			//with tessellation/geometry for triangles
			setGeometryShader(GeometryShader::WIREFRAME);

			myScene->removeObject(0);

			//m_triangle->setMaterial(basicMaterial);
			myScene->addObject(m_triangle);
		}
		else if (currentObject != MESHES::QUAD_MESH && key == 'q')
		{
			currentObject = MESHES::QUAD_MESH;

			//Call this to load the geometry shader adapted to quads
			//(by default, triangle geometry shader is called)
			setGeometryShader(GeometryShader::WIREFRAME, true);

			myScene->removeObject(0);

			//m_quad->setMaterial(basicMaterial);
			myScene->addObject(m_quad);
		}
	}
	else if (v1_basicTeapot)
	{
		//Enable/Disable geometry shader
		if (key == 'g')
		{
			geometry_shader_pass = !geometry_shader_pass;
			setGeometryShader(m_gs);
		}
		//Enable/Disable tessellation
		else if (key == 't')
		{
			tessellation_shader_pass = !tessellation_shader_pass;
			setGeometryShader(GeometryShader::WIREFRAME);
		}

		//By now, if tessellation is enabled, we can only show wireframe
		if (geometry_shader_pass && !tessellation_shader_pass && key == 'w')
		{
			setGeometryShader(GeometryShader::WIREFRAME);
		}
		else if (geometry_shader_pass && !tessellation_shader_pass && key == 'n')
		{
			setGeometryShader(GeometryShader::NORMALS);
		}
		else if (geometry_shader_pass && !tessellation_shader_pass && key == 'p')
		{
			setGeometryShader(GeometryShader::POINTS);
		}
	}
	//Features only available in advanced version
	else if (v2_advancedTeapot)
	{
		if (key == 'f')
		{
			//Disabling tessellation
			tessellation_shader_pass = false;
			setGeometryShader(GeometryShader::WIREFRAME);

			//Removing the current object and adding a new one with the new flat material
			myScene->removeObject(0);
			
			m_teapot->setMaterial(advanced_flat_material);
			myScene->addObject(m_teapot);
		}
		else if (key == 't')
		{
			//Enabling tessellation again
			tessellation_shader_pass = true;
			setGeometryShader(GeometryShader::WIREFRAME);

			//Removing the current object and adding a new one with the new displacement map material
			myScene->removeObject(0);

			m_teapot->setMaterial(advanced_material);
			myScene->addObject(m_teapot);
		}
		else if (key == 'g')
		{
			//Enabling tessellation again
			tessellation_shader_pass = true;
			setGeometryShader(GeometryShader::WIREFRAME);

			//Removing the current object and adding a new one with the new displacement map material
			myScene->removeObject(0);

			m_teapot->setMaterial(advanced_geometry_material);
			myScene->addObject(m_teapot);
		}
	}

	//Common features for v0 and v1: forward rendering and tessellation levels
	if (v1_basicTeapot || v0_triQuadDemo)
	{
		//Enable/Disable forward rendering
		if (key == 'f')
		{
			forward_rendering = !forward_rendering;
		}
		//Increment tess level inner
		else if (key == 'i')
		{
			tess_level_inner += 1.0f;
			myScene->setTessLevelObject(0, tess_level_outer, tess_level_inner);
		}
		//Increment tess level outer
		else if (key == 'o')
		{
			tess_level_outer += 1.0f;
			myScene->setTessLevelObject(0, tess_level_outer, tess_level_inner);
		}
		//Decrement tess level inner
		else if (key == 'k')
		{
			tess_level_inner -= 1.0f;
			if (tess_level_inner < 1.0f) { tess_level_inner = 1.0f; }
			myScene->setTessLevelObject(0, tess_level_outer, tess_level_inner);
		}
		//Decrement tess level outer
		else if (key == 'l')
		{
			tess_level_outer -= 1.0f;
			if (tess_level_outer < 1.0f) { tess_level_outer = 1.0f; }
			myScene->setTessLevelObject(0, tess_level_outer, tess_level_inner);
		}
	}

	//Switch between render versions
	if (!v0_triQuadDemo && key == '0')
	{
		v0_triQuadDemo = true;
		v1_basicTeapot = false;
		v2_advancedTeapot = false;

		//Triangles and quads wireframe works only with both geometry and tessellation enabled
		//because the geometry shader expects to recieve triangles (and they are generated with tessellation)
		geometry_shader_pass = true;
		tessellation_shader_pass = true;
		setGeometryShader(GeometryShader::WIREFRAME);

		//Remove the current object and add a new one with the new material
		myScene->removeObject(0);
		myScene->addObject(m_triangle);

		currentObject = MESHES::TRIANGLE_MESH;

		resetCameraPos();
	}
	else if (!v1_basicTeapot && key == '1')
	{
		v1_basicTeapot = true;
		v2_advancedTeapot = false;
		v0_triQuadDemo = false;

		//Set default scene with only basic forward rendering
		tessellation_shader_pass = false;
		setGeometryShader(GeometryShader::WIREFRAME);

		//Remove the current object and add a new one with the new material
		myScene->removeObject(0);

		m_teapot->setMaterial(basic_material);
		myScene->addObject(m_teapot);

		currentObject = MESHES::TEAPOT_MESH;

		resetCameraPos();
	}
	else if (!v2_advancedTeapot && key == '2')
	{
		v2_advancedTeapot = true;
		v1_basicTeapot = false;
		v0_triQuadDemo = false;

		//Enable forward rendering
		forward_rendering = true;
		//Disable geometry and tessellation shader render
		geometry_shader_pass = false;
		tessellation_shader_pass = false;
		setGeometryShader(GeometryShader::WIREFRAME);

		//Remove the current object and add a new one with the new material
		myScene->removeObject(0);

		m_teapot->setMaterial(advanced_flat_material);
		myScene->addObject(m_teapot);

		currentObject = MESHES::TEAPOT_MESH;

		resetCameraPos();
	}

	//-----Camera controls
	if (key == '+')
	{
		cam_position = cam_position + (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}
	else if (key == '-')
	{
		cam_position = cam_position - (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}
}

void TessellationDemo::MouseFunc(int button, int state, int x, int y)
{
	//Left button click
	if (!mouse_left_button && button == GLUT_LEFT_BUTTON)
	{
		mouse_x_ref = x;
		mouse_y_ref = y;
		mouse_left_button = true;
	}
	//Left button release
	else if (button == GLUT_LEFT_BUTTON && mouse_left_button)
	{
		mouse_left_button = false;
	}
}

void TessellationDemo::MouseMotion(int x, int y)
{
	//Orbital camera
	if (mouse_left_button)
	{
		camera_radius = glm::length(cam_position);

		int x_offset = (x - mouse_x_ref);// *2 * PI / WINDOW_WIDTH;
		int y_offset = (y - mouse_y_ref);// *2 * PI / WINDOW_HEIGHT;

		theta -= glm::radians((float)x_offset);
		phi += glm::radians((float)y_offset);

		if (phi > 1.5f)
		{
			phi = 1.5f;
		}
		else if (phi < -1.5f)
		{
			phi = -1.5f;
		}

		float eye_x = camera_radius * cos(phi) * sin(theta);
		float eye_y = camera_radius * sin(phi);
		float eye_z = camera_radius * cos(phi) * cos(theta);

		myScene->moveOrbitalCamera(glm::vec3(eye_x, eye_y, eye_z), glm::vec3(0, 0, 0), cam_up);

		cam_position = glm::vec3(eye_x, eye_y, eye_z);

		mouse_x_ref = x;
		mouse_y_ref = y;

		//std::cout << eye_x << ", " << eye_y << ", " << eye_z << std::endl;
	}
}

void TessellationDemo::resetCameraPos()
{
	myScene->moveOrbitalCamera(glm::vec3(0, 0, 10), glm::vec3(0, 0, 0), cam_up);
	cam_position = glm::vec3(0, 0, 10);
	theta = 0;
	phi = 0;
}

void TessellationDemo::initializeMaterials()
{	
	//Basic	
	std::unique_ptr<Program> basic_fw_program = std::make_unique<Program>("../Shaders/PGATR/v1_basicForwardRendering.vert",
		"../Shaders/PGATR/v1_basicForwardRendering.frag");
	basic_material = std::make_shared<Material>(std::move(basic_fw_program));
	//Advanced
	std::unique_ptr<Program> advanced_fw_program = std::make_unique<Program>("../Shaders/PGATR/v4_fwTessellation.vert",
		"../Shaders/PGATR/v4_fwTessellation.frag",
		"", "../Shaders/PGATR/v4_fwTessellationLODS.tesc",
		"../Shaders/PGATR/v4_fwTessellation.tese");
	advanced_material = std::make_shared<Material>(std::move(advanced_fw_program));
	advanced_material->createColorTexture("../Images/mapamundi.jpg");
	advanced_material->createEmitTexture("../Images/emissive.png");
	//Advanced flat
	std::unique_ptr<Program> advanced_fw_flat_program = std::make_unique<Program>("../Shaders/PGATR/v4_fwTessellationFlat.vert",
		"../Shaders/PGATR/v4_fwTessellationFlat.frag");
	advanced_flat_material = std::make_shared<Material>(std::move(advanced_fw_flat_program));
	advanced_flat_material->createColorTexture("../Images/mapamundi.jpg");
	advanced_flat_material->createEmitTexture("../Images/emissive.png");
	//Tessellation + geometry stages
	std::unique_ptr<Program> advanced_fw_withGeometry_program = std::make_unique<Program>("../Shaders/PGATR/v5_fwTessellationWithGeometry.vert",
		"../Shaders/PGATR/v5_fwTessellationWithGeometry.frag",
		"../Shaders/PGATR/v5_fwTessellationWithGeometry.geom",
		"../Shaders/PGATR/v4_fwTessellationLODS.tesc",
		"../Shaders/PGATR/v5_fwTessellationWithGeometry.tese");
	advanced_geometry_material = std::make_shared<Material>(std::move(advanced_fw_withGeometry_program));
	advanced_geometry_material->createColorTexture("../Images/mapamundi.jpg");
	advanced_geometry_material->createEmitTexture("../Images/emissive.png");
}

void TessellationDemo::setGeometryShader(GeometryShader gs, bool isQuad)
{
	m_gs = gs;

	std::string vertexPath = "../Shaders/PGATR/v2_geometryRendering.vert";
	std::string fragmentPath = "../Shaders/PGATR/v2_geometryRendering.frag";
	std::string geometryPath = getGeometryShaderPath(m_gs);
	std::string tscPath = "";
	std::string tsePath = "";

	if (tessellation_shader_pass)
	{
		vertexPath = "../Shaders/PGATR/v3_basicInterpolation.vert";
		fragmentPath = "../Shaders/PGATR/v3_basicInterpolation.frag";

		if (!isQuad)
		{
			tscPath = "../Shaders/PGATR/v3_basicInterpolationTri.tesc";
			tsePath = "../Shaders/PGATR/v3_basicInterpolationTri.tese";
		}
		else
		{
			tscPath = "../Shaders/PGATR/v3_basicInterpolationQuad.tesc";
			tsePath = "../Shaders/PGATR/v3_basicInterpolationQuad.tese";
		}

		geometryPath = "../Shaders/PGATR/v3_basicInterpolation.geom";
	}

	std::unique_ptr<Program> gShaderPassProgram = std::make_unique<Program>(vertexPath, fragmentPath,
		geometryPath, tscPath, tsePath);

	current_geometry_material = std::make_shared<Material>(std::move(gShaderPassProgram));
}

std::string TessellationDemo::getGeometryShaderPath(GeometryShader gs)
{
	switch (gs)
	{
	case GeometryShader::NORMALS:
		return "../Shaders/PGATR/v2_geometryRenderingNormals.geom";
		break;
	case GeometryShader::POINTS:
		return "../Shaders/PGATR/v2_geometryRenderingPoints.geom";
		break;
	case GeometryShader::WIREFRAME:
		return "../Shaders/PGATR/v2_geometryRenderingWireframe.geom";
		break;
	}
}