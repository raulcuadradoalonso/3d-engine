#include "stdafx.h"
#include "Demos/SceneDemo.h"

void SceneDemo::CreateScene(int screen_width, int screen_height)
{
	//-----Init scene
	std::unique_ptr<Camera> myCam(new Camera());
	myCam->setView(cam_position, cam_forward, cam_up, orbital_camera);
	myCam->setProjection(fov, (float)screen_width / (float)screen_height, near_plane, far_plane);
	myScene = std::make_unique<Scene>(screen_width, screen_height, std::move(myCam));

	//-----Create materials
	std::shared_ptr<Material> m_cube_material = Material::CreateMaterial(MATERIALS::STANDARD_FR);
	m_cube_material->createColorTexture("../Images/purple_screen.jpg");
	//m_cube_material->createEmitTexture("../Images/Wall/wall_spec.jpg");

	std::shared_ptr<Material> m_plane_material = Material::CreateMaterial(MATERIALS::STANDARD_FR);
	m_plane_material->createColorTexture("../Images/blank_screen.jpg");

	//-----Create objects
	std::shared_ptr<Object> m_cube = Object::createObject(OBJECTS::CUSTOM, m_cube_material, 
		"../Models/HexagonalEnergyField.fbx");
	std::shared_ptr<Object> m_cube2 = Object::createObject(OBJECTS::CUBE, m_cube_material);
	std::shared_ptr<Object> m_plane = Object::createObject(OBJECTS::PLANE, m_plane_material);

	m_cube->scale(glm::vec3(0.04, 0.04, 0.04));
	m_cube2->move(glm::vec3(2, 0, -10));
	m_plane->move(glm::vec3(2, 0, -50));

	myScene->addObject(m_cube);
	//myScene->addObject(m_cube2);
	//myScene->addObject(m_plane);

	m_objects = myScene->getObjects();

	//-----Create lights
	glm::vec3 lPos = glm::vec3(-5, 5, 5);
	glm::vec3 lDiff = glm::vec3(1, 1, 1);
	myScene->addPointLight(std::make_unique<LightPoint>(lPos, lDiff, glm::vec3(1, 1, 1), 10, 1));

	glm::vec3 lPos2 = glm::vec3(5, 5, 5);
	glm::vec3 lDiff2 = glm::vec3(1, 1, 1);
	myScene->addPointLight(std::make_unique<LightPoint>(lPos2, lDiff2, glm::vec3(1, 1, 1), 50, 1));	

	//Create Particle System
	//Emissors
	struct Emisor ePos1;
	ePos1.x = 0;
	ePos1.y = -1;
	ePos1.z = 0;
	ePos1.w = 1;
	std::vector<Emisor> emisors = { ePos1 };

	std::unique_ptr<ParticleSystem> pSystem = std::make_unique<ParticleSystem>();	
	pSystem->initParticleSystem();	
	pSystem->setFireValues(emisors);

	myScene->addParticleSystem(std::move(pSystem));

	//-----Add post-processing
	DepthOfFieldConfig dof;
	dof.focalDistance = -5;
	dof.focusRange = 5;
	dof.maxDistanceFactor = 1 / dof.focusRange;

	myScene->getCam().getPostProcessingStack().SetDepthOfFieldConfig(dof);
}

void SceneDemo::RenderFunc()
{
	//Render objects with forward rendering
	myScene->renderScene();

	glutSwapBuffers();	
}

void SceneDemo::ResizeFunc(int screen_width, int screen_height)
{
	myScene->resizeScene(screen_width, screen_height, fov, near_plane, far_plane);
}

void SceneDemo::IdleFunc()
{
	m_objects[0]->rotate(0.005f, glm::vec3(0, 1, 0));
	myScene->idleScene();
}

void SceneDemo::KeyboardInput(unsigned char key, int x, int y)
{
	//-----Camera controls
	if (key == '+')
	{
		cam_position = cam_position + (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}
	else if (key == '-')
	{
		cam_position = cam_position - (myScene->getCam().getForward() * camZoomSpeed);
		myScene->moveOrbitalCamera(cam_position, glm::vec3(0, 0, 0), cam_up);
	}
}

void SceneDemo::MouseFunc(int button, int state, int x, int y)
{
	if (!mouse_left_button && button == GLUT_LEFT_BUTTON)
	{
		mouse_x_ref = x;
		mouse_y_ref = y;
		mouse_left_button = true;
	}
	else if (button == GLUT_LEFT_BUTTON && mouse_left_button)
	{
		mouse_left_button = false;
	}
}

void SceneDemo::MouseMotion(int x, int y)
{
	//Orbital camera
	if (mouse_left_button)
	{
		camera_radius = glm::length(cam_position);

		int x_offset = (x - mouse_x_ref);// *2 * PI / WINDOW_WIDTH;
		int y_offset = (y - mouse_y_ref);// *2 * PI / WINDOW_HEIGHT;

		theta -= glm::radians((float)x_offset);
		phi += glm::radians((float)y_offset);

		if (phi > 1.5f)
		{
			phi = 1.5f;
		}
		else if (phi < -1.5f)
		{
			phi = -1.5f;
		}

		float eye_x = camera_radius * cos(phi) * sin(theta);
		float eye_y = camera_radius * sin(phi);
		float eye_z = camera_radius * cos(phi) * cos(theta);

		myScene->moveOrbitalCamera(glm::vec3(eye_x, eye_y, eye_z), glm::vec3(0, 0, 0), cam_up);

		cam_position = glm::vec3(eye_x, eye_y, eye_z);

		mouse_x_ref = x;
		mouse_y_ref = y;
	}
}