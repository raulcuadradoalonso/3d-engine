#include "stdafx.h"
#include "Demos/DeferredShadingDemo.h"

void DeferredShadingDemo::CreateScene(int screen_width, int screen_height)
{
	//DEFERRED SHADING
	deferredShading = true;

	//Init scene
	std::unique_ptr<Camera> myCam(new Camera());
	myCam->setView(cam_position, cam_forward, cam_up, orbital_camera);
	myCam->setProjection(fov, (float)screen_width / (float)screen_height, near_plane, far_plane);
	myScene = std::make_unique<Scene>(screen_width, screen_height, std::move(myCam), deferredShading);

	//------Create shaders
	std::unique_ptr<Program> ds_gpass_program = std::make_unique<Program>("../Shaders/DSFirstPass.vert", "../Shaders/DSFirstPass.frag");

	std::unique_ptr<Program> fw_program = std::make_unique<Program>("../Shaders/shader.v0.vert", "../Shaders/shader.v0.frag");

	//------Create materials
	//Set up of deferred shading material
	ds_material = std::make_shared<Material>(std::move(ds_gpass_program));
	ds_material->createColorTexture("../Images/color2.png");
	ds_material->createEmitTexture("../Images/emissive.png");

	//Set up of a forward rendering material
	fw_material = std::make_shared<Material>(std::move(fw_program));
	fw_material->createColorTexture("../Images/color2.png");
	fw_material->createEmitTexture("../Images/emissive.png");

	//------Create objects
	if (deferredShading)
	{
		std::cout << "Setting up Deferred Shading..." << std::endl;

		m_cube = Object::createObject(OBJECTS::CUBE, ds_material);
		myScene->addObject(m_cube);
	}
	else
	{
		std::cout << "Setting up Forward rendering..." << std::endl;

		m_cube = Object::createObject(OBJECTS::CUBE, fw_material);
		myScene->addObject(m_cube);
	}
}

void DeferredShadingDemo::RenderFunc()
{
	myScene->renderScene(deferredShading);
}

void DeferredShadingDemo::ResizeFunc(int screen_width, int screen_height)
{
	myScene->resizeScene(screen_width, screen_height, fov, near_plane, far_plane);
}

void DeferredShadingDemo::IdleFunc()
{
	myScene->idleScene();
}

void DeferredShadingDemo::KeyboardInput(unsigned char key, int x, int y)
{

}

void DeferredShadingDemo::MouseFunc(int button, int state, int x, int y)
{
	if (!mouse_left_button && button == GLUT_LEFT_BUTTON)
	{
		mouse_x_ref = x;
		mouse_y_ref = y;
		mouse_left_button = true;
	}
	else if (button == GLUT_LEFT_BUTTON && mouse_left_button)
	{
		mouse_left_button = false;
	}
}

void DeferredShadingDemo::MouseMotion(int x, int y)
{
	//Orbital camera
	if (mouse_left_button)
	{
		camera_radius = glm::length(cam_position);

		int x_offset = (x - mouse_x_ref);// *2 * PI / WINDOW_WIDTH;
		int y_offset = (y - mouse_y_ref);// *2 * PI / WINDOW_HEIGHT;

		theta -= glm::radians((float)x_offset);
		phi += glm::radians((float)y_offset);

		if (phi > 1.5f)
		{
			phi = 1.5f;
		}
		else if (phi < -1.5f)
		{
			phi = -1.5f;
		}

		float eye_x = camera_radius * cos(phi) * sin(theta);
		float eye_y = camera_radius * sin(phi);
		float eye_z = camera_radius * cos(phi) * cos(theta);

		myScene->moveOrbitalCamera(glm::vec3(eye_x, eye_y, eye_z), glm::vec3(0, 0, 0), cam_up);

		cam_position = glm::vec3(eye_x, eye_y, eye_z);

		mouse_x_ref = x;
		mouse_y_ref = y;
	}
}