#include "stdafx.h"
#include "Light.h"

Light::Light(glm::vec3 diffuse, glm::vec3 specular, float intensity) :
	m_diffuse(diffuse), m_specular(specular), m_intensity(intensity)
{

}

Light::~Light()
{

}

glm::vec3 Light::GetDiffuse() const
{
	return m_diffuse;
}

glm::vec3 Light::GetSpecular() const
{
	return m_specular;
}

float Light::GetIntensity() const
{
	return m_intensity;
}

void Light::SetDiffuse(glm::vec3 diffuse)
{
	m_diffuse = diffuse;
}

void Light::SetSpecular(glm::vec3 specular)
{
	m_specular = specular;
}

void Light::SetIntensity(float i)
{
	m_intensity = i;
}