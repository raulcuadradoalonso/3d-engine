#include "stdafx.h"
#include "Camera.h"
#include "Material.h"
#include "gbuffer.h"
#include "Object.h"

Object::Object(std::unique_ptr<Mesh> mh, std::shared_ptr<Material> &mat)
	: m_material(mat), m_mesh(std::move(mh))
{
	m_model = glm::mat4(1.0f);

	m_tessLevelInner = 3.0f;
	m_tessLevelOuter = 3.0f;
}

Object::Object(const Object& obj) :
	m_mesh(new Mesh(*obj.m_mesh)), m_material(obj.m_material), m_model(obj.m_model),
	m_tessLevelOuter(obj.m_tessLevelOuter), m_tessLevelInner(obj.m_tessLevelInner)
{}

Object& Object::operator=(Object obj)
{
	std::swap(*this, obj);

	return *this;
}

std::unique_ptr<Object> Object::createObject(OBJECTS newObject, std::shared_ptr<Material> &mat, std::string path)
{
	switch (newObject)
	{
	case OBJECTS::TRIANGLE:
		return std::make_unique<Object>(std::move(Mesh::NewMesh(MESHES::TRIANGLE_MESH)), mat);
		break;
	case OBJECTS::QUAD:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::QUAD_MESH), mat);
		break;
	case OBJECTS::PLANE:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::PLANE_MESH), mat);
		break;
	case OBJECTS::CUBE:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::CUBE_MESH), mat);
		break;
	case OBJECTS::SPHERE:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::SPHERE_MESH), mat);
		break;
	case OBJECTS::TEAPOT:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::TEAPOT_MESH), mat);
		break;
	case OBJECTS::CUSTOM:
		return std::make_unique<Object>(Mesh::NewMesh(MESHES::CUSTOM_MESH, path), mat);
		break;
	default:
		std::cerr << "\n Object couldn't be created!" << std::endl;
		return std::unique_ptr<Object>();
		break;
	}
}

void Object::pushUniforms(const Camera &cam, const std::shared_ptr<Material>& currentMat, const bool renderToFBO) const
{
	glm::mat4 modelView = cam.getView() * m_model;
	glm::mat4 modelViewProj = cam.getProjection() * cam.getView() * m_model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));	

	const Program& currentProgram = currentMat->getProgram();

	glUseProgram(currentMat->getProgramID());

	if (currentProgram.getModelViewID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getModelViewID(), 1, GL_FALSE, &(modelView[0][0]));
	}

	if (currentProgram.getModelMatrixID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getModelMatrixID(), 1, GL_FALSE, &(m_model[0][0]));
	}

	if (currentProgram.getModelViewProjectionID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getModelViewProjectionID(), 1, GL_FALSE,
			&(modelViewProj[0][0]));
	}

	if (currentProgram.getNormalMatID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getNormalMatID(), 1, GL_FALSE,
			&(normal[0][0]));
	}

	if (currentProgram.getProjectionMatID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getProjectionMatID(), 1, GL_FALSE,
			&(cam.getProjection()[0][0]));
	}

	if (currentProgram.getViewMatrixID() != -1)
	{
		glUniformMatrix4fv(currentProgram.getViewMatrixID(), 1, GL_FALSE,
			&(cam.getView()[0][0]));
	}

	if (currentProgram.getColorTextureID() != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, currentMat->getColorTextureBufferID());
		glUniform1i(currentProgram.getColorTextureID(), 0);
	}

	if (currentProgram.getEmitTextureID() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, currentMat->getEmiTextureBufferID());
		glUniform1i(currentProgram.getEmitTextureID(), 1);
	}

	if (currentProgram.getSpecTextureID() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, currentMat->getSpecTextureBufferID());
		glUniform1i(currentProgram.getSpecTextureID(), 2);
	}

	if (currentProgram.getNormalTextureID() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 3);
		glBindTexture(GL_TEXTURE_2D, currentMat->getNormalTextureBufferID());
		glUniform1i(currentProgram.getNormalTextureID(), 3);
	}

	if (currentProgram.getTessLevelOuter() != -1)
	{
		glUniform1f(currentProgram.getTessLevelOuter(), m_tessLevelOuter);
	}

	if (currentProgram.getTessLevelInner() != -1)
	{
		glUniform1f(currentProgram.getTessLevelInner(), m_tessLevelInner);
	}

	if (currentProgram.getRenderToFboID() != -1)
	{
		glUniform1f(currentProgram.getRenderToFboID(), renderToFBO);
	}
}

//Mat: 
// - Sometimes it is needed to render an object first with a different shader than the one it has attached
//so if mat is not null, it is used to render the object instead of the currently attached one
void Object::renderObject(const Camera &cam, const std::shared_ptr<Material>& mat, const bool drawPoints) const
{	
	const std::shared_ptr<Material> &currentMat = (mat == nullptr) ? m_material : mat;

	pushUniforms(cam, currentMat, cam.hasPostProcessing());
	
	//Render using materials with tessellation shaders
	if (currentMat->getProgram().hasTessellation())
	{
		drawTessellatedObject();
	}
	//Basic render
	else
	{
		drawObject(drawPoints);
	}
}

//Render screen plane is used when you want to use a plane to add post-process effects or deferred rendering. 
// - The desired material must be attached before the call to this function
void Object::renderScreenQuad() const
{
	drawObject(false);
}

void Object::deferredShadingRender(const GBuffer &gBuffer)
{
	glUseProgram(m_material->getProgramID());

	const Program& currentProgram = m_material->getProgram();

	if (currentProgram.getDSposID() != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gBuffer.getPositionBufferID());
		glUniform1i(currentProgram.getDSposID(), 0);
	}

	if (currentProgram.getDSnormalID() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, gBuffer.getNormalBufferID());
		glUniform1i(currentProgram.getDSnormalID(), 1);
	}

	if (currentProgram.getDScolorID() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, gBuffer.getAlbedoSpecBufferID());
		glUniform1i(currentProgram.getDScolorID(), 2);
	}	

	glBindVertexArray(m_mesh->getVAOindex());
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void Object::drawObject(const bool drawPoints) const
{
	glBindVertexArray(m_mesh->getVAOindex());

	GLenum triangle_element = GL_TRIANGLES;
	if (drawPoints) { triangle_element = GL_POINTS; }

	switch (m_mesh->getTypeOfMesh())
	{
	case MESHES::TRIANGLE_MESH:		
		glDrawArrays(triangle_element, 0, 3);
		break;
	case MESHES::QUAD_MESH:				
		if (!drawPoints) { triangle_element = GL_TRIANGLE_STRIP; }
		glDrawArrays(triangle_element, 0, 4);
		break;
	default:
		glDrawElements(triangle_element, m_mesh->getNumFaces() * 3, GL_UNSIGNED_INT, (void*)0);
		break;
	}	
}

void Object::drawTessellatedObject() const
{
	glBindVertexArray(m_mesh->getVAOindex());

	switch (m_mesh->getTypeOfMesh())
	{
	case MESHES::TRIANGLE_MESH:
		glPatchParameteri(GL_PATCH_VERTICES, 3);
		glDrawArrays(GL_PATCHES, 0, 3);
		break;
	case MESHES::QUAD_MESH:
		glPatchParameteri(GL_PATCH_VERTICES, 4);
		glDrawArrays(GL_PATCHES, 0, 4);
		break;
	default:
		glPatchParameteri(GL_PATCH_VERTICES, 3);
		glDrawElements(GL_PATCHES, m_mesh->getNumFaces() * 3, GL_UNSIGNED_INT, (void*)0);
		break;
	}
}

void Object::setTessLevelInner(const float tli)
{
	m_tessLevelInner = tli;
}

void Object::setTessLevelOuter(const float tlo)
{
	m_tessLevelOuter = tlo;
}

void Object::move(const glm::vec3 translation)
{
	m_model = glm::translate(m_model, translation);
}

void Object::rotate(const float angle, const glm::vec3 axis)
{
	m_model = glm::rotate(m_model, angle, axis);
}

void Object::scale(const glm::vec3 s)
{
	m_model = glm::scale(m_model, s);
}

void Object::setMaterial(std::shared_ptr<Material> &m)
{
	m_material = m;
}

Material& Object::getMaterial() const
{
	return *m_material;
}

glm::mat4 Object::getModel() const
{
	return m_model;
}

Mesh& Object::getMesh() const
{
	return *m_mesh;
}

std::shared_ptr<Material> Object::swapMaterial(std::shared_ptr<Material> &m)
{
	std::shared_ptr<Material> currentMaterial(std::move(m_material));
	m_material = m;
	return currentMaterial;
}