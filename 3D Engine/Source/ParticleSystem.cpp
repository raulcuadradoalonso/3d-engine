#include "stdafx.h"
#include "ParticleSystem.h"

void ParticleSystem::initParticleSystem()
{
	isAnimateParticlesActive = false;

	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesFire.comp"));
	csProgramID = csProgram->getProgramID();

	renderingProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/billboardRendering.vert",
		"../Shaders/PGATR/Compute Shaders/billboardRendering.frag"));

	renderingProgramID = renderingProgram->getProgramID();

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &posSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct pos), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &velSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, velSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct vel), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &colSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, colSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct color), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &defaultTimeSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultTimeSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct defaultTime), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &currentTimeSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, currentTimeSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct currentTime), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &defaultEmissorPosSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultEmissorPosSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct DefaultPos), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &defaultVelSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultVelSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct defaultVel), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glGenBuffers(1, &defaultColSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultColSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, NUM_PARTICLES * sizeof(struct defaultCol), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindVertexArray(0);
}

void ParticleSystem::resetParticles(std::vector<Emisor> emisors)
{
	float numOfParticlesPerEmissor = (float)NUM_PARTICLES / (float)emisors.size();

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultEmissorPosSSbo);
	struct DefaultPos *defaultEmissorPositions = (struct DefaultPos *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct DefaultPos), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		defaultEmissorPositions[i].x = emisors[(int)(i / numOfParticlesPerEmissor)].x + ranf(XMIN, XMAX);
		defaultEmissorPositions[i].y = emisors[(int)(i / numOfParticlesPerEmissor)].y + ranf(YMIN, YMAX);
		defaultEmissorPositions[i].z = emisors[(int)(i / numOfParticlesPerEmissor)].z + ranf(ZMIN, ZMAX);
		defaultEmissorPositions[i].w = 1;
	}

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
	struct pos *points = (struct pos *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct pos), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		points[i].x = defaultEmissorPositions[i].x;
		points[i].y = defaultEmissorPositions[i].y;
		points[i].z = defaultEmissorPositions[i].z;
		points[i].w = 1.;
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, velSSbo);
	struct vel *vels = (struct vel *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct vel), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		vels[i].vx = ranf(VXMIN, VXMAX);
		vels[i].vy = ranf(VYMIN, VYMAX);
		vels[i].vz = ranf(VZMIN, VZMAX);
		vels[i].vw = 0.;
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultVelSSbo);
	struct defaultVel *defaultVels = (struct defaultVel *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct defaultVel), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		defaultVels[i].vx = vels[i].vx;
		defaultVels[i].vy = vels[i].vy;
		defaultVels[i].vz = vels[i].vz;
		defaultVels[i].vw = vels[i].vw;		
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, colSSbo);
	struct color *colors = (struct color *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct color), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		colors[i].r = 1.0f;
		colors[i].g = 1.0f;
		colors[i].b = 1.0f;
		colors[i].a = 1.;
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultColSSbo);
	struct defaultCol *defColors = (struct defaultCol *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct defaultCol), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		defColors[i].r = colors[i].r;
		defColors[i].g = colors[i].g;
		defColors[i].b = colors[i].b;
		defColors[i].a = colors[i].a;
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, defaultTimeSSbo);
	struct defaultTime *defaultTimes = (struct defaultTime *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct defaultTime), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		defaultTimes[i].t = ranf(MINTIME, MAXTIME);
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, currentTimeSSbo);
	struct currentTime *currentTimes = (struct currentTime *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, NUM_PARTICLES * sizeof(struct currentTime), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	for (int i = 0; i < NUM_PARTICLES; i++)
	{
		currentTimes[i].t = defaultTimes[i].t;
	}
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, posSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, velSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, colSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 7, defaultTimeSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, currentTimeSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 9, defaultEmissorPosSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, defaultVelSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 11, defaultColSSbo);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void ParticleSystem::drawParticles(const Camera &cam)
{	

	glUseProgram(renderingProgramID);
	glPointSize(5.0f);

	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 modelView = cam.getView() * model;
	glm::mat4 modelViewProj = cam.getProjection() * cam.getView() * model;	

	if (renderingProgram->getModelViewID() != -1)
	{
		glUniformMatrix4fv(renderingProgram->getModelViewID(), 1, GL_FALSE, &(modelView[0][0]));
	}

	if (renderingProgram->getModelViewProjectionID() != -1)
	{
		glUniformMatrix4fv(renderingProgram->getModelViewProjectionID(), 1, GL_FALSE,
			&(modelViewProj[0][0]));
	}

	if (renderingProgram->getProjectionMatID() != -1)
	{
		glUniformMatrix4fv(renderingProgram->getProjectionMatID(), 1, GL_FALSE,
			&(cam.getProjection()[0][0]));
	}

	GLint inPosLoc = glGetAttribLocation(renderingProgramID, "inPos");
	GLint inColorLoc = glGetAttribLocation(renderingProgramID, "inColor");

	glBindVertexArray(vao);

	//Positions
	glEnableVertexAttribArray(inPosLoc);
	glBindBuffer(GL_ARRAY_BUFFER, posSSbo);
	glVertexAttribPointer(inPosLoc, 4, GL_FLOAT, GL_FALSE, 0, 0);

	//Colors
	glEnableVertexAttribArray(inColorLoc);
	glBindBuffer(GL_ARRAY_BUFFER, colSSbo);
	glVertexAttribPointer(inColorLoc, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_POINTS, 0, NUM_PARTICLES);	

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ParticleSystem::animateParticles()
{
	if (!isAnimateParticlesActive) { return; }
	glUseProgram(csProgramID);
	glDispatchCompute(NUM_PARTICLES / WORK_GROUP_SIZE, 1, 1);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

float ParticleSystem::ranf(float low, float high)
{
	long random();		// returns integer 0 - TOP

	float r = (float)rand();
	return(low + r * (high - low) / (float)RAND_MAX);
}

void ParticleSystem::setDefaultValues()
{
	//Position ranges
	XMIN = -1;
	XMAX = 1;
	YMIN = -1;
	YMAX = 1;
	ZMIN = -1;
	ZMAX = 1;

	//Define emissors
	struct Emisor ePos1;
	ePos1.x = 0;
	ePos1.y = 0;
	ePos1.z = 0;
	ePos1.w = 1;
	std::vector<Emisor> emisors = { ePos1 };

	isAnimateParticlesActive = false;

	resetParticles(emisors);
}

void ParticleSystem::setExplosionValues()
{
	//Position ranges
	XMIN = -10.0;
	XMAX = 10.0;
	YMIN = -10.0;
	YMAX = 10.0;
	ZMIN = -10.0;
	ZMAX = 10.0;

	//Velocity ranges
	VXMIN = -100.;
	VXMAX = 100.;
	VYMIN = -100.;
	VYMAX = 100.;
	VZMIN = -100.;
	VZMAX = 100.;

	//Emissors
	struct Emisor ePos1;
	ePos1.x = 0;
	ePos1.y = 0;
	ePos1.z = 0;
	ePos1.w = 1;
	std::vector<Emisor> emisors = { ePos1 };

	//Set default program
	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesDefault.comp"));
	csProgramID = csProgram->getProgramID();

	//Enable animation
	isAnimateParticlesActive = true;

	//Reset buffers
	resetParticles(emisors);
}

void ParticleSystem::setWaterValues()
{
	//Position ranges
	XMIN = -10.0;
	XMAX = 10.0;
	YMIN = -10.0;
	YMAX = 10.0;
	ZMIN = -10.0;
	ZMAX = 10.0;

	//Velocity ranges
	VXMIN = 10.;
	VXMAX = 50;
	VYMIN = -10.;
	VYMAX = 10.;
	VZMIN = -10.;
	VZMAX = 10.;

	//Emissors
	struct Emisor ePos1;

	ePos1.x = 0;
	ePos1.y = 0;
	ePos1.z = 0;
	ePos1.w = 1;

	std::vector<Emisor> emisors = { ePos1 };

	//Set default program
	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesDefault.comp"));
	csProgramID = csProgram->getProgramID();

	//Enable animation
	isAnimateParticlesActive = true;

	//Reset buffers
	resetParticles(emisors);
}

void ParticleSystem::setFireValues(std::vector<Emisor> emisors)
{
	//Ranges
	XMIN = -1;
	XMAX = 1;
	YMIN = -0;
	YMAX = 0;
	ZMIN = -1;
	ZMAX = 1;

	VXMIN = -1.5;
	VXMAX = 1.5;
	VYMIN = -1.5;
	VYMAX = 1.5;
	VZMIN = -1.5;
	VZMAX = 1.5;

	//Set fire program
	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesFire.comp"));
	csProgramID = csProgram->getProgramID();

	//Enable animation
	isAnimateParticlesActive = true;

	//Reset buffers
	resetParticles(emisors);
}

void ParticleSystem::setAtractorsDemo()
{
	//Ranges
	XMIN = -10.0;
	XMAX = 10.0;
	YMIN = -10.0;
	YMAX = 10.0;
	ZMIN = -10.0;
	ZMAX = 10.0;

	VXMIN = -10.;
	VXMAX = 10.;
	VYMIN = -10.;
	VYMAX = 10.;
	VZMIN = -10.;
	VZMAX = 10.;

	//Emissors
	struct Emisor ePos1;
	struct Emisor ePos2;
	struct Emisor ePos3;

	ePos1.x = 0;
	ePos1.y = 0;
	ePos1.z = 0;
	ePos1.w = 1;

	ePos2.x = -100;
	ePos2.y = 0;
	ePos2.z = 0;
	ePos2.w = 1;

	ePos3.x = 100;
	ePos3.y = 0;
	ePos3.z = 0;
	ePos3.w = 1;

	std::vector<Emisor> emisors = { ePos1, ePos2, ePos3 };

	//Set fire program
	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesAtractors.comp"));
	csProgramID = csProgram->getProgramID();

	//Enable animation
	isAnimateParticlesActive = true;

	//Reset buffers
	resetParticles(emisors);
}

void ParticleSystem::setSphereDemo()
{
	//Position ranges
	XMIN = -100.0;
	XMAX = 100.0;
	YMIN = -100.0;
	YMAX = 100.0;
	ZMIN = -100.0;
	ZMAX = 100.0;

	//Velocity ranges
	VXMIN = -10.;
	VXMAX = 10.;
	VYMIN = -10.;
	VYMAX = 10.;
	VZMIN = -10.;
	VZMAX = 10.;

	//Emissors
	struct Emisor ePos1;
	ePos1.x = 0;
	ePos1.y = 0;
	ePos1.z = 0;
	ePos1.w = 1;
	std::vector<Emisor> emisors = { ePos1 };

	//Set default program
	csProgram.reset(new Program("../Shaders/PGATR/Compute Shaders/particlesSphere.comp"));
	csProgramID = csProgram->getProgramID();

	//Enable animation
	isAnimateParticlesActive = true;

	//Reset buffers
	resetParticles(emisors);
}