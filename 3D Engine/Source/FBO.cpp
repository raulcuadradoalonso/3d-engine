#include "stdafx.h"
#include "Material.h"
#include "FBO.h"

FBO::FBO()
{
	glGenFramebuffers(1, &m_fbo);
	glGenTextures(1, &m_colorBufferID);
	glGenTextures(1, &m_depthBufferID);
	glGenTextures(1, &m_vertexBufferID);
}

FBO::~FBO()
{
	glDeleteFramebuffers(1, &m_fbo);
	glDeleteTextures(1, &m_colorBufferID);
	glDeleteTextures(1, &m_depthBufferID);
	glDeleteTextures(1, &m_vertexBufferID);
}

void FBO::resizeFBO(unsigned int w, unsigned int h)
{
	glBindTexture(GL_TEXTURE_2D, m_colorBufferID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	glBindTexture(GL_TEXTURE_2D, m_vertexBufferID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	glBindTexture(GL_TEXTURE_2D, m_depthBufferID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, m_colorBufferID, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_TEXTURE_2D, m_vertexBufferID, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		m_depthBufferID, 0);

	const GLenum buffs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, buffs);

	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		std::cerr << "Error setting up the FBO" << std::endl;
		exit(-1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FBO::pushUniforms(const Material &mat) const
{
	const Program& currentProgram = mat.getProgram();

	if (currentProgram.getColorFboTex() != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_colorBufferID);
		glUniform1i(currentProgram.getColorFboTex(), 0);
	}

	if (currentProgram.getVertexFboTex() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, m_vertexBufferID);
		glUniform1i(currentProgram.getVertexFboTex(), 1);
	}

	if (currentProgram.getDepthFboTex() != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, m_depthBufferID);
		glUniform1i(currentProgram.getDepthFboTex(), 2);
	}
}

const GLuint FBO::getFboID() const
{
	return m_fbo;
}

const GLuint FBO::getColorBuffID() const
{
	return m_colorBufferID;
}

const GLuint FBO::getDepthBuffID() const
{
	return m_depthBufferID;
}

const GLuint FBO::getVertexBuffID() const
{
	return m_vertexBufferID;
}