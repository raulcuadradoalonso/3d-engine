#include "stdafx.h"
#include "Material.h"
#include "Object.h"
#include "PostProcessingStack.h"
#include "Camera.h"

PostProcessingStack::PostProcessingStack()
{
	
}

PostProcessingStack::PostProcessingStack(const PostProcessingStack& pps)
{
	m_dof = std::make_unique<DepthOfFieldConfig>(*pps.m_dof);
}

void PostProcessingStack::PushPostProcessingUniforms(const Material& mat) const
{
	const Program& currentProgram = mat.getProgram();

	//Boolean states

	if (currentProgram.getFocalDistance() != -1)
	{
		glUniform1f(currentProgram.getFocalDistance(), m_dof->focalDistance);
	}

	if (currentProgram.getMaxDistanceFactor() != -1)
	{
		glUniform1f(currentProgram.getMaxDistanceFactor(), m_dof->maxDistanceFactor);
	}	
}

void PostProcessingStack::SetDepthOfFieldConfig(const DepthOfFieldConfig& dof)
{
	m_dof = std::make_unique<DepthOfFieldConfig>(dof);
}

const DepthOfFieldConfig* PostProcessingStack::GetDepthOfFieldConfig() const
{
	return m_dof.get();
}